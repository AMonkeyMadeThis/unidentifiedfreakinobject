﻿namespace UnidentifiedFreakinObject.CapturePhoto.Scripts
{
	using AMonkeyMadeThis.Common.Controller;
	using Shared.Model;
	using UnityEngine;

	[RequireComponent(typeof(Renderer))]
	///
	/// <summary>CameraRenderer</summary>
	///
	public class CameraRenderer : AbstractController
	{
		public GameObject plane;

		private WebCamTexture texture;

		public override void OnEnable()
		{
			base.OnEnable();
			
			ShowWebCamDevice(CameraModel.Instance.DeviceName);
		}

		public override void OnDisable()
		{
			base.OnDisable();

			StopWebCamDevice();
		}

		public override void AddListeners()
		{
			base.AddListeners();

			AddModelListeners();
		}

		public override void RemoveListeners()
		{
			base.RemoveListeners();

			RemoveModelListeners();
		}

		private void AddModelListeners()
		{
			CameraModel.CameraDeviceNameChanged += HandleCameraDeviceNameChanged;
		}

		private void RemoveModelListeners()
		{
			CameraModel.CameraDeviceNameChanged -= HandleCameraDeviceNameChanged;
		}

		private void HandleCameraDeviceNameChanged(string deviceName)
		{
			ShowWebCamDevice(deviceName);
		}

		public void ShowWebCamDevice(string deviceName)
		{
			StopWebCamDevice();

			texture = new WebCamTexture(deviceName);
			GetComponent<Renderer>().material.mainTexture = texture;
			texture.Play();
		}

		public void StopWebCamDevice()
		{
			if (texture != null)
			{
				texture.Stop();
				texture = null;
			}
		}
	}
}