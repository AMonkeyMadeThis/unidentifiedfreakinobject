﻿namespace AMonkeyMadeThis.UnityFlex.UI.Component.Data.Layouts
{
	using Enum;
	using UnityEngine;

	[RequireComponent(typeof(DataGroup))]
    /**
     * Layout
     */
    public abstract class Layout : UIComponent
    {
        /**
         * LayoutType
         */
        public LayoutType LayoutType = LayoutType.Random;
        /**
         * UpdateMode
         */
        public UpdateMode UpdateMode = UpdateMode.Manual;
        /**
         * DataGroup
         */
        protected DataGroup DataGroup;
		
        /**
         * CollectComponents
         */
        protected override void CollectComponents()
        {
			base.CollectComponents();

            DataGroup = GetComponent<DataGroup>();
        }

        /**
         * Update
         */
        public void Update()
        {
            if (UpdateMode == UpdateMode.Update)
                ApplyLayout();
        }

        /**
         * LateUpdate
         */
        public void LateUpdate()
        {
            if (UpdateMode == UpdateMode.LateUpdate)
                ApplyLayout();
        }

        /**
         * FixedUpdate
         */
        public void FixedUpdate()
        {
            if (UpdateMode == UpdateMode.FixedUpdate)
                ApplyLayout();
        }

        /**
         * ApplyLayout
         */
        public virtual void ApplyLayout()
        {
            int numChildren = transform.childCount;

            if (numChildren > 0)
            {
                for (int i = 0; i < numChildren; i++)
                {
                    Transform child = transform.GetChild(i);

                    if (child)
                        AlignChild(child, i);
                }
            }   
        }

        protected virtual void AlignChild(Transform child, int childIndex)
        {
            child.localScale = Vector3.one;
            child.localRotation = Quaternion.Euler(0f, 0f, 0f);
            child.localPosition = Vector3.zero;
        }
    }
}
