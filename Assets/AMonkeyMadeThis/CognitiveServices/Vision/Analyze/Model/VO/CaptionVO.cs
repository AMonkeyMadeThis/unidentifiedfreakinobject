﻿namespace AMonkeyMadeThis.CognitiveServices.Vision.Analyze.Model.VO
{
	using Common.Model.VO;
	using System;

	[Serializable]
	/**
	 * CaptionVO
	 */
	public class CaptionVO : ValueObject
	{
		public string text;
		public float confidence;
	}
}
