﻿namespace UnidentifiedFreakinObject.CapturePhoto.Scripts
{
	using Shared.Model;
	using Shared.Scripts;
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;

	/**
	 * CapturePhoto
	 */
	public class CapturePhoto : AbstractView
	{
		public Camera Camera;
		public Vector2 ImageSize = new Vector2(1920, 1080);
		public int ColourDepth = 24;

		private bool captureImage = false;
		
		public void CaptureImage()
		{
			captureImage = true;
		}

		void LateUpdate()
		{
			if (captureImage)
			{
				StartCoroutine(RenderCameraToTexture());
				captureImage = false;
			}
		}

		private IEnumerator RenderCameraToTexture()
		{
			yield return new WaitForEndOfFrame();

			var texture = new Texture2D((int)ImageSize.x, (int)ImageSize.y, TextureFormat.ARGB32, false);
			
			RenderCameraToImage(Camera, ref texture);

			CameraModel.Instance.RawImage = texture;

			GoToCropImage();
		}

		private void RenderCameraToImage(Camera camera, ref Texture2D image)
		{
			RenderTexture rt = new RenderTexture(image.width, image.height, 32, RenderTextureFormat.ARGB32);
			camera.targetTexture = rt;
			camera.Render();
			RenderTexture.active = rt;
			image.ReadPixels(new Rect(0, 0, image.width, image.height), 0, 0);
			RenderTexture.active = null;
			Destroy(rt);
			
			byte[] bytes = image.EncodeToPNG();
			image.LoadImage(bytes);
		}
		
		public static string ScreenShotName(int width, int height)
		{
			return string.Format("{0}/screenshots/screen_{1}x{2}_{3}.png",
								 Application.dataPath,
								 width, height,
								 System.DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss"));
		}

		private void SaveToFile(Texture2D image)
		{
			int width = Mathf.RoundToInt(ImageSize.x);
			int height = Mathf.RoundToInt(ImageSize.y);
			string filename = ScreenShotName(width, height);
			byte[] bytes = image.EncodeToPNG();
			System.IO.File.WriteAllBytes(filename, bytes);

			Debug.Log("Saved " + filename);
		}

		private IEnumerator Upload(Texture2D image)
		{
			byte[] bytes = image.EncodeToPNG();
			string uploadURL = "https://api.projectoxford.ai/vision/v1.0/analyze?visualFeatures=Categories,Tags,Description,Faces";
			WWWForm postForm = new WWWForm();
			postForm.AddBinaryData("file", bytes, "0001.png", "image/png");
			Dictionary<string, string> headers = postForm.headers;
			headers["Ocp-Apim-Subscription-Key"] = "d735b2d76b944b30969451c0f233a850";
			WWW upload = new WWW(uploadURL, postForm.data, headers);
			yield return upload;
			if (upload.error == null)
			{
				Debug.Log(upload.text);
			}
			else
			{
				Debug.Log("Error during upload: " + upload.error);
			}

			captureImage = false;
		}
	}
}
