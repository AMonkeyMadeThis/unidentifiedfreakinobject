﻿namespace AMonkeyMadeThis.Common.Controller.Behaviours
{
	/**
	 * DoNotSleep
	 */
	public class DoNotDestroy : AbstractController
	{
		public override void Awake()
		{
			base.Awake();

			DontDestroyOnLoad(this.gameObject);
		}
	}
}
