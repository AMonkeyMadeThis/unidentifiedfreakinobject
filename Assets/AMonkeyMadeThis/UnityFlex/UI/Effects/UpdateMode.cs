﻿namespace Enum
{
    using System;

    [Flags]
    /**
     * UpdateMode
     */
    public enum UpdateMode
    {
        Manual,
        Update,
        FixedUpdate,
        LateUpdate
    }
}
