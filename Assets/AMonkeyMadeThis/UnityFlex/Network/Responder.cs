﻿namespace AMonkeyMadeThis.UnityFlex.Network
{
	/**
	 * Responder
	 */
	public class Responder
	{
		public delegate void ResultHandler(object result);

		public delegate void FaultHandler(object fault);

		public ResultHandler Result;

		public FaultHandler Fault;

		public Responder(ResultHandler result = null, FaultHandler fault = null)
		{
			if (result != null)
				this.Result = result;

			if (fault != null)
				this.Fault = fault;
		}
	}
}
