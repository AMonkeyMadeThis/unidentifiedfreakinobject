﻿using UnityEngine;
using System.Collections;

public class UploadImage : MonoBehaviour
{
	// Use this for initialization
	void Start()
	{
	}

	// Update is called once per frame
	void Update()
	{
	}

	void OnGUI()
	{
		GUI.Label(new Rect(100, 0, 500, 20), Application.dataPath);
		if (GUI.Button(new Rect(100, 100, 150, 20), "Upload"))
		{
			UploadFile("http://localhost/image.php");
		}
	}

	IEnumerator UploadFileCo(string uploadURL)
	{
		WWW localFile = new WWW("file://G:/0001.png");
		yield return localFile;
		WWWForm postForm = new WWWForm();
		postForm.AddBinaryData("file", localFile.bytes, "0001.png", "image/png");
		WWW upload = new WWW(uploadURL, postForm);
		yield return upload;
		if (upload.error == null)
		{
			Debug.Log(upload.text);
		}
		else
		{
			Debug.Log("Error during upload: " + upload.error);
		}
	}

	void UploadFile(string uploadURL)
	{
		StartCoroutine(UploadFileCo(uploadURL));
	}
}
