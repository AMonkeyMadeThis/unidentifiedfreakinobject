﻿namespace AMonkeyMadeThis.UnityFlex.UI.Effects
{
    using System.Collections;
    using UnityEngine;

    /**
     * EffectIn
     */
    public class EffectIn : MonoBehaviour
    {
        /**
         * StartPositionOffset
         */
        public Vector3 StartPositionOffset = Vector3.zero;
        /**
         * StartRotationOffset
         */
        public Vector3 StartRotationOffset = Vector3.zero;
        /**
         * StartScaleOffset
         */
        public Vector3 StartScaleOffset = Vector3.zero;
        /**
         * StartDelay
         */
        public float StartDelay = 0f;
        /**
         * Duration
         */
        public float Duration = 0f;

        /**
         * OriginalPosition
         */
        private Vector3 OriginalPosition;
        /**
         * OriginalRotation
         */
        private Vector3 OriginalRotation;
        /**
         * OriginalScale
         */
        private Vector3 OriginalScale;

        /**
         * StartPosition
         */
        private Vector3 StartPosition;
        /**
         * StartRotation
         */
        private Vector3 StartRotation;
        /**
         * StartScale
         */
        private Vector3 StartScale;
        /**
         * EndPosition
         */
        private Vector3 EndPosition;
        /**
         * EndRotation
         */
        private Vector3 EndRotation;
        /**
         * EndScale
         */
        private Vector3 EndScale;

        /**
         * Start
         */
        void Start()
        {
            Play();
        }

        public void Play()
        {
            RecordOriginalTransforms();
            CalculateStartTransforms();

            StartCoroutine("Animate");
        }

        private void RecordOriginalTransforms()
        {
            EndPosition = OriginalPosition = transform.localPosition;
            EndRotation = OriginalRotation = transform.localRotation.eulerAngles;
            EndScale = OriginalScale = transform.localScale;
        }

        private void CalculateStartTransforms()
        {
            StartPosition = EndPosition + StartPositionOffset;
            StartRotation = EndRotation + StartRotationOffset;
            StartScale = EndScale + StartScaleOffset;
        }

        private IEnumerator Animate()
        {
            UpdateTransform(StartPosition, StartRotation, StartScale);

            yield return new WaitForSeconds(StartDelay);

            double startTime = Time.realtimeSinceStartup;
            double endTime = startTime + Duration;

            while (Time.realtimeSinceStartup < endTime)
            {
                float progress = (float)((Time.realtimeSinceStartup - startTime) / Duration);

                Vector3 position = Vector3.Lerp(StartPosition, EndPosition, progress);
                Vector3 rotation = Vector3.Lerp(StartRotation, EndRotation, progress);
                Vector3 scale = Vector3.Lerp(StartScale, EndScale, progress);

                UpdateTransform(position, rotation, scale);

                yield return new WaitForEndOfFrame();
            }

            UpdateTransform(EndPosition, EndRotation, EndScale);
        }

        private void UpdateTransform(Vector3 position, Vector3 rotation, Vector3 scale)
        {
            // position
            transform.localPosition = position;
            // rotation
            Quaternion _rotation = new Quaternion();
            _rotation.eulerAngles = rotation;
            transform.localRotation = _rotation;
            // scale
            transform.localScale = scale;
        }
    }
}
