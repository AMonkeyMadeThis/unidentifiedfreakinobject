﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;
using UnidentifiedFreakinObject.Shared.Model;

[RequireComponent(typeof(Dropdown))]
/// <summary>CameraDropdownController</summary>
public class CameraDropdownController : MonoBehaviour
{
	public CameraController CameraController;
	private Dropdown dropdown;
	private List<Dropdown.OptionData> options;

	///<summary>Use this for initialization</summary>
	public void Start ()
	{
		dropdown = GetComponent<Dropdown>();
		PopulateDatasource();
		AddListeners();
		SelectDefaultOption();
	}

	private void SelectDefaultOption()
	{
		HandleCamerasDropdownValueChanged(GetOptionByIndex(0));
	}

	private void PopulateDatasource()
	{
		WebCamDevice[] devices = WebCamTexture.devices;

		dropdown.ClearOptions();

		if (dropdown != null && devices != null && devices.Length > 0)
		{
			options = new List<Dropdown.OptionData>();

			foreach (var device in devices)
			{
				Dropdown.OptionData option = new Dropdown.OptionData();
				option.text = device.name;

				options.Add(option);
			}

			dropdown.AddOptions(options);
		}
	}

	public void AddListeners()
	{
		if (dropdown != null)
		{
			dropdown.onValueChanged.AddListener(delegate {
				HandleCamerasDropdownValueChanged(GetOptionByIndex(dropdown.value));
			});
		}
	}

	private void HandleCamerasDropdownValueChanged(Dropdown.OptionData selected)
	{
		if (selected != null && selected.text != null && selected.text.Length > 0)
		{
			string deviceName = selected.text;

			CameraModel.Instance.DeviceName = deviceName;
		}
	}

	private Dropdown.OptionData GetOptionByIndex(int index)
	{
		try
		{
			return options[index];
		}
		catch (System.Exception)
		{
			return null;
		}
	}
}
