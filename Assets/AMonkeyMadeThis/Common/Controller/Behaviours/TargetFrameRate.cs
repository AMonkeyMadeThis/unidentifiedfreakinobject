﻿namespace AMonkeyMadeThis.Common.Controller.Behaviours
{
	using UnityEngine;

	/**
	 * TargetFrameRate
	 */
	public class TargetFrameRate : AbstractController
	{
		public int FrameRate = 30;

		public override void Awake()
		{
			base.Awake();

			Application.targetFrameRate = FrameRate;
		}
	}
}
