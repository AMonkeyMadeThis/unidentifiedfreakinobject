﻿namespace AMonkeyMadeThis.UnityFlex.Localisation.UI.Component.Data
{
	using Controller;
	using Enum;
	using Model;
	using UnityFlex.UI.Component;

	/**
	 * LocaleDropDown
	 */
	public class LocaleDropDown : DropDownController
	{
		public override void Start()
		{
			base.Start();

			UpdateDataSource();
		}

		#region Listeners

		/**
		 * AddListeners
		 */
		public override void AddListeners()
		{
			base.AddListeners();

			LocalisationController.OnLocalisationAvailable += HandleLocalisationAvailable;
		}

		#endregion

		#region Handlers

		/**
		 * HandleLocalisationAvailable
		 */
		private void HandleLocalisationAvailable()
		{
			UpdateDataSource();
		}

		/**
		 * HandleDropDownValueChanged
		 */
		protected override void HandleDropDownValueChanged()
		{
			base.HandleDropDownValueChanged();
			
			LocaleChain selected = (LocaleChain)SelectedItem;
			LocaleCode locale = selected.Locale;
			LocalisationController.Instance.Current = locale;
		}
		
		#endregion

		#region Population

		/**
		 * UpdateDataSource
		 */
		private void UpdateDataSource()
		{
			this.Datasource = LocalisationController.Instance.Available;
			MakeInitialSelection();
		}

		#endregion

		#region Selection

		protected override void MakeInitialSelection()
		{
			base.MakeInitialSelection();

			if (Datasource != null && Datasource.Count > 0)
			{
				SelectedIndex = IndexOfLocale(LocalisationController.Instance.Current);
			}
		}

		private int IndexOfLocale(LocaleCode target)
		{
			foreach (var chain in Datasource)
			{
				var _chain = chain as LocaleChain;

				if (_chain != null && _chain.Locale == target)
					return Datasource.IndexOf(_chain);
			}

			return -1;
		}

		#endregion
	}
}
