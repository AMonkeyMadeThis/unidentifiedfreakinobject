﻿namespace AMonkeyMadeThis.UnityFlex.UI.Component.Data.Layouts
{
    /**
     * Inbuilt layouts don't play nice with touchy feely input and can't be switched at runtime so ...
     */
    public enum LayoutType
    {
        Random,
        Grid,
        Stack,
        StackTL,
        StackTR,
        StackBL,
        StackBR,
        Vertical,
        Horizontal,
        Ring
    }
}
