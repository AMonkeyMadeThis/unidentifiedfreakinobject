﻿namespace AMonkeyMadeThis.CognitiveServices.Vision.Analyze.Model.VO
{
	using Common.Model.VO;
	using System;

	[Serializable]
	/**
	 * MetadataVO
	 */
	public class MetadataVO : ValueObject
	{
		public int width;
		public int height;
		public string format;
	}
}
