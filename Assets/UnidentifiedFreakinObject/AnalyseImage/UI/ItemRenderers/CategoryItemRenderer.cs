﻿namespace UnidentifiedFreakinObject.AnalyseImage.UI.ItemRenderers
{
	using AMonkeyMadeThis.CognitiveServices.Vision.Analyze.Model.VO;
	using AMonkeyMadeThis.UnityFlex.Localisation.Controller;

	/**
	 * CategoryItemRenderer
	 */
	public class CategoryItemRenderer : ImageAnalysisItemRenderer
	{
		private CategoryVO CategoryData
		{
			get { return Data as CategoryVO; }
		}
		
		protected override void PopulateUI()
		{
			if (CategoryData != null)
			{
				UpdateNameLabel("CognitiveServices.vision.analyse.category." + CategoryData.name);
				PopulateConfidenceLabel(CategoryData.score);
			}
		}

		protected override void UpdateNameLabel(string key)
		{
			if (NameLabel == null)
				return;

			string localised = LocalisationController.Instance.LocaliseString(key, "CognitiveServices");

			NameLabel.text = localised;
		}
	}
}
