﻿namespace UnidentifiedFreakinObject.Shared.Scripts
{
	using AMonkeyMadeThis.Ads.Controller;
	using AMonkeyMadeThis.Common.Controller;
	using AMonkeyMadeThis.FaceBook.Controller;
	using AMonkeyMadeThis.GoogleAnalytics.Controller;
	using AMonkeyMadeThis.UnityFlex.UI.Component.Application;
	using UnityEngine;
	using UnityEngine.Advertisements;
	using UnityEngine.UI;

	/**
	 * AbstractView
	 */
	public abstract class AbstractView : AbstractController
	{
		public Button BackButton;
		public Button NextButton;

		public void FacebookLogIn()
		{
			FaceBookController.Instance.LogIn();
		}

		public void FacebookLogOut()
		{
			FaceBookController.Instance.LogOut();
		}
		
		public void GoBack()
		{
			ModuleLoader.Instance.GoBack();
		}

		public virtual void GoNext()
		{
			// template method
		}

		public void GoToHome()
		{
			ModuleLoader.Instance.Load("Home");
		}

		public void GoToCapturePhoto()
		{
			ModuleLoader.Instance.Load("CapturePhoto");
		}

		public void GoToCropImage()
		{
			ModuleLoader.Instance.Load("CropImage");
		}

		public void GoToAnalyseImage()
		{
			ModuleLoader.Instance.Load("AnalyseImage");
		}

		public void GoToShareImage()
		{
			ModuleLoader.Instance.Load("ShareImage");
		}

		public void GoToSettings()
		{
			ModuleLoader.Instance.Load("Settings");
		}

		public void ShowAd()
		{
			UnityAdsController.Instance.ShowAd(HandleUnityAdsCallback);
		}
		
		protected virtual void HandleUnityAdsCallback(ShowResult result)
		{
			switch (result)
			{
				case ShowResult.Finished:
				{
					GoogleAnalyticsController.Instance.Tracker.LogEvent(new EventHitBuilder().SetEventCategory("Advert").SetEventAction("Finished"));
					HandleAdvertFinished();
					break;
				}
				case ShowResult.Skipped:
				{
					GoogleAnalyticsController.Instance.Tracker.LogEvent(new EventHitBuilder().SetEventCategory("Advert").SetEventAction("Skipped"));
					HandleAdvertSkipped();
					break;
				}
				case ShowResult.Failed:
				{
					GoogleAnalyticsController.Instance.Tracker.LogEvent(new EventHitBuilder().SetEventCategory("Advert").SetEventAction("Failed"));
					HandleAdvertFailed();
					break;
				}
			}
		}

		protected virtual void HandleAdvertFinished()
		{
			Debug.Log("AdvertFinished");
		}

		protected virtual void HandleAdvertSkipped()
		{
			Debug.Log("AdvertSkipped");
		}

		protected virtual void HandleAdvertFailed()
		{
			Debug.Log("AdvertFailed");
		}

		protected void DisableNextButton()
		{
			if (NextButton != null)
				NextButton.interactable = false;
		}

		protected void EnableNextButton()
		{
			if (NextButton != null)
				NextButton.interactable = true;
		}
	}
}
