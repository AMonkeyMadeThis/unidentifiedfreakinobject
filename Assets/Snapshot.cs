﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Snapshot : MonoBehaviour
{
	public Camera Camera;
	public int resWidth = 1920;
	public int resHeight = 1080;
	public Text Output;

	private bool takeHiResShot = false;

	public static string ScreenShotName(int width, int height)
	{
		return string.Format("{0}/screenshots/screen_{1}x{2}_{3}.png",
							 Application.dataPath,
							 width, height,
							 System.DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss"));
	}

	public void TakeHiResShot()
	{
		takeHiResShot = true;
	}

	private bool IsRunning = false;

	void LateUpdate()
	{
		if (takeHiResShot && !IsRunning)
		{
			IsRunning = true;
			StartCoroutine(CaptureAndUpload());
		}
	}

	private IEnumerator CaptureAndUpload()
	{
		RenderTexture rt = new RenderTexture(resWidth, resHeight, 24);
		Camera.targetTexture = rt;
		Texture2D screenShot = new Texture2D(resWidth, resHeight, TextureFormat.RGB24, false);
		Camera.Render();
		RenderTexture.active = rt;
		screenShot.ReadPixels(new Rect(0, 0, resWidth, resHeight), 0, 0);
		Camera.targetTexture = null;
		RenderTexture.active = null; // JC: added to avoid errors
		Destroy(rt);
		byte[] bytes = screenShot.EncodeToPNG();
		/*
		string filename = ScreenShotName(resWidth, resHeight);
		System.IO.File.WriteAllBytes(filename, bytes);
		Debug.Log(string.Format("Took screenshot to: {0}", filename));
		takeHiResShot = false;
		*/
		string uploadURL = "https://api.projectoxford.ai/vision/v1.0/analyze?visualFeatures=Categories,Tags,Description,Faces";
		WWWForm postForm = new WWWForm();
		postForm.AddBinaryData("file", bytes, "0001.png", "image/png");
		Dictionary<string, string> headers = postForm.headers;
		headers["Ocp-Apim-Subscription-Key"] = "d735b2d76b944b30969451c0f233a850";
		WWW upload = new WWW(uploadURL, postForm.data, headers);
		yield return upload;
		if (upload.error == null)
		{
			Debug.Log(upload.text);
			Output.text = upload.text;
		}
		else
		{
			Debug.Log("Error during upload: " + upload.error);
		}

		takeHiResShot = false;
		IsRunning = false;
	}
}
