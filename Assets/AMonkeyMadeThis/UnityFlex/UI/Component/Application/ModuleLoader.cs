﻿namespace AMonkeyMadeThis.UnityFlex.UI.Component.Application
{
	using Common.Controller;
	using GoogleAnalytics.Controller;
	using System.Collections;
	using UnityEngine.SceneManagement;

	/**
	 * ModuleLoader
	 */
	public class ModuleLoader : AbstractController
	{
		private Stack History;

		private static ModuleLoader _instance;

		public static ModuleLoader Instance
		{
			get { return _instance; }
			private set
			{
				_instance = value;
			}
		}

		public ModuleLoader()
		{
			Instance = this;
		}

		public override void Awake()
		{
			base.Awake();

			History = new Stack();
		}

		public void Load(string moduleName)
		{
			if (!string.IsNullOrEmpty(moduleName))
			{
				History.Push(SceneManager.GetActiveScene().name);
				LoadScene(moduleName);
			}
		}

		public void GoBack()
		{
			if (History.Count > 0)
			{
				var moduleName = History.Pop() as string;

				if (!string.IsNullOrEmpty(moduleName))
					LoadScene(moduleName);
			}
		}

		private void LoadScene(string moduleName)
		{
			GoogleAnalyticsController.Instance.Tracker.LogScreen(moduleName);
			SceneManager.LoadScene(moduleName);
		}
	}
}
