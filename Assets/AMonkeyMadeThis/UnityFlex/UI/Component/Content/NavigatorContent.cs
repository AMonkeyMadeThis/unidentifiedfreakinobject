﻿namespace AMonkeyMadeThis.UnityFlex.UI.Component.Content
{
	/**
	 * NavigatorContent
	 */
	public class NavigatorContent : UIComponent
	{
		/**
		 * Label
		 */
		public string Label;
		/**
		 * ShowInMenu
		 */
		public bool ShowInMenu = true;
    }
}
