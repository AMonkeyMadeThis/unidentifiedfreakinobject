﻿namespace AMonkeyMadeThis.Common.Service
{
	using Controller;
	using UnityFlex.Network;

	/**
	 * AbstractService
	 */
	public class AbstractService : AbstractController
	{
		protected virtual void SendResult(Responder responder, object result = null)
		{
			if (responder != null && responder.Result != null)
				responder.Result(result);
		}

		protected virtual void SendFault(Responder responder, object fault = null)
		{
			if (responder != null && responder.Fault != null)
				responder.Fault(fault);
		}
	}
}
