﻿namespace AMonkeyMadeThis.UnityFlex.UI.Component.Navigation
{
	using AMonkeyMadeThis.UnityFlex.UI.Component;
	using Content;
	using UnityEngine;
	using UnityEngine.UI;

	[RequireComponent(typeof(ToggleGroup))]
	/**
	 * NavBar
	 */
	public class NavBar : UIComponent
	{
		/**
		 * ButtonPrototype
		 */
		public NavBarButton ButtonPrototype;

		private ViewStack _viewStack;

		/**
		 * ViewStack
		 */
		public ViewStack ViewStack
		{
			get { return _viewStack; }
			set
			{
				_viewStack = value;
				UpdateChildren();
			}
		}

		[SerializeField]
		/**
		 * SelectedIndex
		 */
		private int _selectedIndex = -1;
		/**
		 * SelectedIndex
		 */
		public int SelectedIndex
		{
			get { return _selectedIndex; }
			set
			{
				_selectedIndex = value;
				UpdateChildren();
			}
		}

		/**
		 * ToggleGroup
		 */
		private ToggleGroup ToggleGroup;

		protected override void CollectComponents()
		{
			base.CollectComponents();

			ToggleGroup = GetComponent<ToggleGroup>();
		}

		private void UpdateChildren()
		{
			if (ViewStack != null && ViewStack.Views != null && ViewStack.Views.Length > 0)
				CreateViews(ViewStack.Views);
		}

		private void CreateViews(NavigatorContent[] views)
		{
			if (ButtonPrototype != null)
			{
				int n = views.Length;

				for (int i = 0; i < n; i++)
				{
					NavigatorContent view = views[i];

					if (view != null && view.ShowInMenu)
					{
						NavBarButton instance = Instantiate(ButtonPrototype) as NavBarButton;
						instance.Index = i;
						instance.View = view;
						instance.Text = view.Label;
						instance.Selected = i == SelectedIndex;

						InitToggle(instance);
						InitTransform(instance);
					}
				}
			}
		}

		private void InitToggle(NavBarButton instance)
		{
			Toggle toggle = instance.GetComponent<Toggle>();

			if (toggle != null)
			{
				toggle.group = ToggleGroup;
				toggle.onValueChanged.AddListener(delegate { HandleToggleChanged(instance); });
			}
		}
		
		private void HandleToggleChanged(NavBarButton instance)
		{
			ViewStack.GoToLabel(instance.Text);
		}

		private void InitTransform(NavBarButton instance)
		{
			instance.transform.parent = this.transform;
			instance.transform.localPosition = Vector3.zero;
			instance.transform.localRotation = Quaternion.Euler(Vector3.zero);
			instance.transform.localScale = Vector3.one;
		}
	}
}
