﻿namespace AMonkeyMadeThis.CognitiveServices.Vision.Analyze.Model
{
	using System;
	using VO;

	[Serializable]
	/**
	 * AnalyseResult
	 */
	public class AnalyseResult
	{
		public CategoryVO[] categories;
		public TagVO[] tags;
		public DescriptionVO description;
		public FaceVO[] faces;
		public MetadataVO metadata;
	}
}
