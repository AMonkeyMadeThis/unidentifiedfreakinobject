﻿namespace AMonkeyMadeThis.CognitiveServices.Vision.Analyze.Model.VO
{
	using Common.Model.VO;
	using System;

	[Serializable]
	/**
	 * CategoryVO
	 */
	public class CategoryVO : ValueObject
	{
		public string name;
		public float score;
	}
}
