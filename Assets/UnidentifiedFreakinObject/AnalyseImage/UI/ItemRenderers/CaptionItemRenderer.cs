﻿namespace UnidentifiedFreakinObject.AnalyseImage.UI.ItemRenderers
{
	using AMonkeyMadeThis.CognitiveServices.Vision.Analyze.Model.VO;

	/**
	 * CaptionItemRenderer
	 */
	public class CaptionItemRenderer : ImageAnalysisItemRenderer
	{
		private CaptionVO CaptionData
		{
			get { return Data as CaptionVO; }
		}

		protected override void PopulateUI()
		{
			if (CaptionData != null)
			{
				UpdateNameLabel(CaptionData.text);
				PopulateConfidenceLabel(CaptionData.confidence);
			}
		}
	}
}
