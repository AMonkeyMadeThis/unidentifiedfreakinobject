﻿namespace AMonkeyMadeThis.UnityFlex.UI.Component.Navigation
{
	using Content;
	using UnityEngine;
	using UnityEngine.UI;

	/**
	 * NavBarButton
	 */
	public class NavBarButton : UIComponent
	{
		private int _index;
		/**
		 * Index
		 */
		public int Index
		{
			get { return _index; }
			set
			{
				_index = value;
				InvalidateProperties();
			}
		}

		private NavigatorContent _view;
		/**
		 * View
		 */
		public NavigatorContent View
		{
			get { return _view; }
			set
			{
				_view = value;
				InvalidateProperties();
			}
		}

		private string _text;
		/**
		 * Text
		 */
		public string Text
		{
			get { return _text; }
			set
			{
				_text = value;
				InvalidateProperties();
			}
		}

		[SerializeField]
		/**
		 * Selected
		 */
		private bool _selected;
		/**
		 * Selected
		 */
		public bool Selected
		{
			get { return _selected; }
			set
			{
				_selected = value;
				InvalidateProperties();
			}
		}

		/**
		 * Label
		 */
		protected Text Label;
		/**
		 * Toggle
		 */
		protected Toggle Toggle;

		protected override void CollectComponents()
		{
			Label = GetComponent<Text>();
			Toggle = GetComponent<Toggle>();
		}

		public override void AddListeners()
		{
			base.AddListeners();

			AddToggleListeners();
		}

		private void AddToggleListeners()
		{
			if (Toggle != null)
				Toggle.onValueChanged.AddListener(delegate { Selected = Toggle.isOn; InvalidateProperties(); });
		}
		
		protected override void CommitProperties()
		{
			SetLabelText();
			SetToggleSelected();

			base.CommitProperties();
		}

		private void SetToggleSelected()
		{
			if (Toggle != null)
			{
				Toggle.isOn = Selected;
				Toggle.targetGraphic.color = (Toggle.isOn) ? Toggle.colors.highlightedColor : Toggle.colors.normalColor;
			}
		}

		protected virtual void SetLabelText()
		{
			if (Label != null)
				Label.text = Text;
		}
	}
}
