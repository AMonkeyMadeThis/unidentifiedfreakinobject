﻿namespace AMonkeyMadeThis.UnityFlex.UI.Component.Content
{
	using Common.Model.VO;
	using Data;
	using Data.ItemRenderers;
	using System;
	using UnityEngine;

	/**
	 * Page
	 */
	public abstract class Page : NavigatorContent
    {
        #region Events

        /**
		 * delegate NavigateBackAction
		 */
        public delegate void NavigateBackAction();
        /**
		 * OnNavigateBack
		 */
        public event NavigateBackAction OnNavigateBack;
        /**
		 * delegate NavigatePreviousAction
		 */
        public delegate void NavigatePreviousAction();
        /**
		 * OnNavigatePrevious
		 */
        public event NavigatePreviousAction OnNavigatePrevious;
        /**
		 * delegate NavigateNextAction
		 */
        public delegate void NavigateNextAction();
        /**
		 * OnNavigateNext
		 */
        public event NavigateNextAction OnNavigateNext;

        #endregion

        #region Vars

        private ValueObject[] _dataSource;
		/**
		 * dataSource
		 */
		public ValueObject[] dataSource
		{
			get { return (container != null) ? container.dataSource : null; }
			set
			{
                if (container != null)
                    container.dataSource = value;
			}
		}

		/**
		 * itemRenderers
		 */
		public ItemRenderer[] itemRenderers;

        #endregion

        #region Components

        /**
         * container
         */
        public DataGroup container;

        #endregion

        #region Lifecycle

        /**
         * CollectComponents
         */
        protected override void CollectComponents()
        {
            base.CollectComponents();
            container = GetComponentInChildren<DataGroup>();
            container.ItemRendererFunction = ItemRendererFunction;
        }

        /**
         * OnDisable
         */
        public override void OnDisable()
        {
            base.OnDisable();
            dataSource = null;
        }

        #endregion

        #region Listeners

        /**
         * AddListeners
         */
        public override void AddListeners()
        {
            base.AddListeners();
            AddContainerListeners();
        }

        /**
         * RemoveListeners
         */
        public override void RemoveListeners()
        {
            base.RemoveListeners();
            RemoveContainerListeners();
        }

        /**
         * AddContainerListeners
         */
        private void AddContainerListeners()
        {
            if (container != null)
            {
                container.OnItemSelected += HandleOnItemSelected;
                container.OnContainerEmpty += HandleOnContainerEmpty;
            }
        }

        /**
         * RemoveContainerListeners
         */
        private void RemoveContainerListeners()
        {
            if (container != null)
            {
                container.OnItemSelected -= HandleOnItemSelected;
                container.OnContainerEmpty -= HandleOnContainerEmpty;
            }
        }

        #endregion

        #region Handlers

        /**
         * HandleOnItemSelected
         */
        private void HandleOnItemSelected(ItemRenderer itemRenderer)
        {
            ItemSelected(itemRenderer);
        }

        /**
         * HandleOnContainerEmpty
         */
        private void HandleOnContainerEmpty()
        {
            DispatchNavigateBack();
        }

        /**
         * HandleOnItemRendererTapped
         */
        private void HandleOnItemRendererTapped(ItemRenderer itemRenderer)
        {
            ItemSelected(itemRenderer);
        }

		#endregion

		#region Data

		/**
		 * set data provider, allowing a place to override functionality to add title objects for example
		 */
		protected virtual void updateDataProvider(ValueObject[] dataSource)
		{
			this.dataSource = dataSource;
		}

		#endregion

		#region ItemRenderers

		/**
         * ItemRendererFunction allows a DataGroup to delegate its requests for an item renderer
         * so that the components controlling it are able to configure the ItemRenderers used
         * to display the contents of the datasource.
         * 
         * However, as unity uses prefabs and we can't just instantiate a script we need to hold
         * an array of the prefb'd item renderers.
         */
		protected virtual ItemRenderer ItemRendererFunction(ValueObject item)
        {
			/*
            if (item is AudioVO)
            {
                return SearchItemRenderersForType(typeof(AudioItemRenderer));
            }
			*/

			return null;
        }

        /**
         * SearchItemRenderersForType
         */
        protected ItemRenderer SearchItemRenderersForType(Type type)
        {
			Debug.Log(this + " SearchItemRenderersForType("+ type.ToString() + ")");

            foreach (var itemRenderer in itemRenderers)
			{
				Debug.Log(this + " Checking " + itemRenderer.GetType().ToString() + " matches "+ type.ToString());

				if (itemRenderer.GetType() == type)
				{
					Debug.Log(this + " Selected " + itemRenderer.ToString());

					return itemRenderer;
				}
            }

            return null;
        }
        
        /**
         * ItemSelected
         */
        protected virtual void ItemSelected(ItemRenderer itemRenderer)
        {
            Debug.Log(this + " ItemSelected "+ itemRenderer);
        }

        #endregion

        #region Dispatch Event

        protected void DispatchNavigateBack()
        {
            if (OnNavigateBack != null)
                OnNavigateBack();
        }

        protected void DispatchNavigatePrevious()
        {
            if (OnNavigatePrevious != null)
                OnNavigatePrevious();
        }

        protected void DispatchNavigateNext()
        {
            if (OnNavigateNext != null)
                OnNavigateNext();
        }

        #endregion
    }
}