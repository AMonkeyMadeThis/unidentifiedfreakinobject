﻿namespace AMonkeyMadeThis.UnityFlex.UI.Component
{
	using Localisation.Controller;
	using UnityEngine;

	/**
	 * UIComponent
	 */
	public abstract class UIComponent : MonoBehaviour
	{
		#region Vars

		[SerializeField]
		/**
		 * (default) ResourceBundle
		 */
		private string _resourceBundle = "Application";

		/**
		 * ResourceBundle
		 */
		public string ResourceBundle
		{
			get { return _resourceBundle; }
			set
			{
				UnRegisterResourceBundleUse();
				_resourceBundle = value;
				RegisterResourceBundleUse();
				InvalidateProperties();
			}
		}

		[HideInInspector]
		/**
		 * IsValid
		 */
		public bool IsValid = true;

		#endregion

		#region Lifecycle

		/**
         * OnEnable
         */
		public virtual void OnEnable()
        {
            //Debug.Log(this + " OnEnable");

            AddListeners();
			InvalidateProperties();
        }

        /**
         * Awake
         */
        public virtual void Awake()
        {
            CollectComponents();
			RegisterResourceBundleUse();
		}

        /**
         * CollectComponents
         */
        protected virtual void CollectComponents()
        {
            //Debug.Log(this + " CollectComponents");
        }

        /**
         * Start
         */
        public virtual void Start()
        {
			//Debug.Log(this + " Start");
			InvalidateProperties();
        }

        /**
         * OnDisable
         */
        public virtual void OnDisable()
        {
            //Debug.Log(this + " OnDisable");

            RemoveListeners();
        }

		#endregion

		#region Localisation
		
		private void RegisterResourceBundleUse()
		{
			if (ResourceBundle != null && ResourceBundle.Length > 0)
				LocalisationController.Instance.RegisterResourceBundleUse(ResourceBundle);
		}

		private void UnRegisterResourceBundleUse()
		{
			if (ResourceBundle != null && ResourceBundle.Length > 0)
				LocalisationController.Instance.UnRegisterResourceBundleUse(ResourceBundle);
		}

		#endregion

		#region Invalidation

		public virtual void InvalidateProperties()
		{
			if (IsValid)
			{
				IsValid = false;
				Invoke("CommitProperties", 0.1f);
			}
		}

		protected virtual void CommitProperties()
		{
			IsValid = true;
		}

		#endregion

		#region Listeners

		/**
         * OnEnable
         */
		public virtual void AddListeners()
        {
			//Debug.Log(this + " AddListeners");

			AddLocalisationListeners();
		}

        /**
         * RemoveListeners
         */
        public virtual void RemoveListeners()
        {
			//Debug.Log(this + " RemoveListeners");

			RemoveLocalisationListeners();
		}

		private void AddLocalisationListeners()
		{
			LocalisationController.OnLocaleChanged += HandleOnLocaleChanged;
		}

		private void RemoveLocalisationListeners()
		{
			LocalisationController.OnLocaleChanged -= HandleOnLocaleChanged;
		}

		#endregion

		#region Handlers

		protected virtual void HandleOnLocaleChanged()
		{
			InvalidateProperties();
		}

		#endregion
	}
}
