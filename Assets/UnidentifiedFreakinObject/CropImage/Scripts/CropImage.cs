﻿namespace UnidentifiedFreakinObject.CropImage.Scripts
{
	using Shared.Model;
	using System.Collections;
	using UnidentifiedFreakinObject.Shared.Scripts;
	using UnityEngine;

	/**
	 * CropImage
	 */
	public class CropImage : AbstractView
	{
		public Camera Camera;
		public Vector2 ImageSize = new Vector2(1920, 1080);
		public int ColourDepth = 24;

		private bool captureImage = false;

		public Transform Canvas;

		private Vector3 InitialPosition;
		private Quaternion InitialRotation;
		private Vector3 InitialScale;
		
		public override void Start()
		{
			base.Start();

			SetInitialSettings();
		}

		void LateUpdate()
		{
			if (captureImage)
			{
				StartCoroutine(RenderCameraToTexture());
				captureImage = false;
			}
		}

		private void SetInitialSettings()
		{
			if (Canvas != null)
			{
				InitialPosition = Canvas.localPosition;
				InitialRotation = Canvas.localRotation;
				InitialScale = Canvas.localScale;
			}
		}

		public void CaptureImage()
		{
			captureImage = true;
		}

		private IEnumerator RenderCameraToTexture()
		{
			yield return new WaitForEndOfFrame();

			var texture = new Texture2D((int)ImageSize.x, (int)ImageSize.y, TextureFormat.ARGB32, false);

			RenderCameraToImage(Camera, ref texture);

			CameraModel.Instance.CroppedImage = texture;

			GoToAnalyseImage();
		}

		private void RenderCameraToImage(Camera camera, ref Texture2D image)
		{
			RenderTexture rt = new RenderTexture(image.width*2, image.height*2, 32, RenderTextureFormat.ARGB32);
			camera.targetTexture = rt;
			camera.Render();
			RenderTexture.active = rt;
			image.ReadPixels(camera.pixelRect, 0, 0);
			RenderTexture.active = null;
			Destroy(rt);

			byte[] bytes = image.EncodeToPNG();
			image.LoadImage(bytes);
		}
		
		public void Reset()
		{
			if (Canvas != null)
			{
				Canvas.localPosition = InitialPosition;
				Canvas.localRotation = InitialRotation;
				Canvas.localScale = InitialScale;
			}
		}
	}
}
