﻿namespace AMonkeyMadeThis.Util
{
	using System;
	using System.IO;
	using System.Xml.Serialization;

	/**
     * XmlUtils
     */
	public class XmlUtils
	{
		/**
         * Serialize type to string
         */
		public static string Serialize(Type type, object data)
		{
			StringWriter writer = new StringWriter();
			XmlSerializer serialiser = new XmlSerializer(type);
			serialiser.Serialize(writer, data);
			writer.Close();

			string result = writer.ToString();
			
			return result;
		}

		/**
         * Deserialize string to type
         */
		public static object Deserialize(Type type, string raw)
		{
			XmlSerializer serialiser = new XmlSerializer(type);
			Stream stream = StringUtils.ConvertDataStringToStream(raw);
			object result = serialiser.Deserialize(stream);

			return result;
		}
	}
}
