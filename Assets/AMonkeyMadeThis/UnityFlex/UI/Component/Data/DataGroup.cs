﻿namespace AMonkeyMadeThis.UnityFlex.UI.Component.Data
{
	using Common.Model.VO;
	using Component;
	using Effects;
	using ItemRenderers;
	using Layouts;
	using System.Collections.Generic;
	using TouchScript.Gestures;
	using UnityEngine;

	/**
     * DataGroup
     */
	public class DataGroup : UIComponent
    {
        #region Delegates

        /**
         * ItemRendererFunction
         */
        public delegate ItemRenderer ItemRendererFunctionDelegate(ValueObject item);
        /**
         * ItemRendererFunction
         */
        public ItemRendererFunctionDelegate ItemRendererFunction;

        #endregion

        #region Events

        /**
         * ItemSelectedAction
         */
        public delegate void ItemSelectedAction(ItemRenderer itemRenderer);
        /**
         * OnItemSelected
         */
        public event ItemSelectedAction OnItemSelected;
        /**
         * ContainerEmptyAction
         */
        public delegate void ContainerEmptyAction();
        /**
         * OnContainerEmpty
         */
        public event ContainerEmptyAction OnContainerEmpty;

        #endregion

        #region Vars

        private ValueObject[] _dataSource;
        /**
		 * dataSource
		 */
        public ValueObject[] dataSource
        {
            get { return _dataSource; }
            set
            {
                removeItemRenderers();
                _dataSource = value;
                createItemRenderers();
            }
        }

        private int _selectedIndex = -1;
        /**
		 * SelectedIndex
		 */
        public int SelectedIndex
        {
            get { return _selectedIndex; }
            set
            {
                _selectedIndex = value;
                UpdateRendererSelection();
            }
        }

        private ItemRenderer _selectedItem;
        /**
		 * SelectedIndex
		 */
        public ItemRenderer SelectedItem
        {
            get { return (SelectedIndex > -1 && SelectedIndex < ItemRenderers.Count) ? ItemRenderers[SelectedIndex] : null; }
            set
            {
                SelectedIndex = ItemRenderers.IndexOf(value);
                UpdateRendererSelection();
            }
        }

        public int Width = 500;
        public int Height = 500;
        
        private LayoutType _layout = LayoutType.Grid;
        /**
		 * LayoutType
		 */
        public LayoutType Layout
        {
            get { return _layout; }
            set
            {
                _layout = value;
                updateLayouts();
            }
        }

        /**
         * Layouts
         */
        public Layout[] Layouts;
        /**
         * ItemRenderers
         */
        public List<ItemRenderer> ItemRenderers;

        #endregion

        #region Lifecycle

        protected override void CollectComponents()
        {
            base.CollectComponents();
            Layouts = GetComponents<Layout>();
        }

        #endregion

        #region Item Renderers
        
        private void UpdateRendererSelection()
        {
            if (ItemRenderers != null)
            {
                ItemRenderer selected = SelectedItem;

                foreach (var itemRenderer in ItemRenderers)
                {
                    itemRenderer.Selected = itemRenderer == selected;
                }
            }
        }

        public ItemRenderer GetTopItemRenderer()
        {
            int lastChildIndex = transform.childCount - 1;

            if (lastChildIndex > -1)
            {
                Transform child = transform.GetChild(lastChildIndex);

                if (child != null)
                {
                    ItemRenderer itemRenderer = child.GetComponent<ItemRenderer>();

                    if (itemRenderer != null)
                        return itemRenderer;
                }
            }

            return null;
        }

        /**
         * GetItemRendererDisplayingItem
         */
        public ItemRenderer GetItemRendererDisplayingItem(ValueObject item)
        {
            if (ItemRenderers != null)
            {
                foreach (var itemRenderer in ItemRenderers)
                {
                    if (itemRenderer.Data == item)
                        return itemRenderer;
                }
            }

            return null;
        }

        /**
         * createItemRenderers
         */
        protected virtual void createItemRenderers()
        {
            if (dataSource != null)
            {
                if (ItemRenderers == null)
                    ItemRenderers = new List<ItemRenderer>();

                ItemRenderers.Clear();

                foreach (var item in dataSource)
                {
                    ItemRenderer itemRenderer = createItemRenderer(item);

					if (itemRenderer != null)
					{
						itemRenderer.Data = item;

						ItemRenderers.Add(itemRenderer);
						addChild(itemRenderer);
						ApplyCurrentLayout();
					}
                }
            }
        }

        /**
         * createItemRenderer
         */
        protected virtual ItemRenderer createItemRenderer(ValueObject item)
        {
            ItemRenderer prototype = ItemRendererFunction(item);

            if (prototype)
            {
                ItemRenderer instance = Instantiate(prototype.gameObject).GetComponent<ItemRenderer>();
                addItemListeners(instance);

                return instance;
            }

            return null;
        }

        /**
         * removeItemRenderers, tries to remove all children from this container. Make sure to use a suitable number of 'tries'
         * to avoid infinite loop so uses 3x the length of the datasource. If this component is placed on the same object as a
         * 'page' for example the page will be enabled/disabled which will make unity fail to remove the children as it cannot do
         * so while the parent is toggling its enabled, however grandparent is fine.
         */
        public void removeItemRenderers()
        {
            int tries = (dataSource != null) ? dataSource.Length * 3 : 100;
            
            while (transform.childCount > 0 && --tries>0)
            {
                ItemRenderer itemRenderer = transform.GetChild(0).GetComponent<ItemRenderer>();
                removeItemRenderer(itemRenderer);
            }
        }

        /**
         * removeItemRenderer, overload to enable dispatching a message if the last child is removed. We can't do this every time
         * as when the previous page is cleared in the background it will push us back to the index, so we can never navigate past
         * the index due to the index being cleared. This method allows us to differentiate a user removing the item.
         */
        private void removeItemRenderer(ItemRenderer itemRenderer, bool dispatchEventWhenLastRendererRemoved)
        {
            removeItemRenderer(itemRenderer);

            if (dispatchEventWhenLastRendererRemoved && transform.childCount == 0)
                dispatchOnContainerEmpty();
        }

        private void removeItemRenderer(ItemRenderer itemRenderer)
        {
            if (itemRenderer != null)
            {
                if (ItemRenderers.Contains(itemRenderer))
                    ItemRenderers.Remove(itemRenderer);

                removeItemListeners(itemRenderer);
                itemRenderer.transform.parent = null;
                DestroyImmediate(itemRenderer.gameObject);
                updateLayouts();
            }
        }

        #endregion

        public void addChild(ItemRenderer itemRenderer)
		{
            int childIndex = transform.childCount - 1;

            itemRenderer.transform.SetParent(transform);
            AdjustEffectIn(itemRenderer, childIndex);
		}

        private void addItemListeners(ItemRenderer itemRenderer)
        {
            if (itemRenderer != null)
            {
                itemRenderer.OnItemRendererTapped += HandleOnItemRendererTapped;
                itemRenderer.OnItemRendererClose += HandleOnItemRendererClose;

                var pressGesture = itemRenderer.GetComponent<PressGesture>();
                if (pressGesture != null) pressGesture.Pressed += pressedHandler;
            }
        }

        private void removeItemListeners(ItemRenderer itemRenderer)
        {
            if (itemRenderer != null)
            {
                itemRenderer.OnItemRendererTapped -= HandleOnItemRendererTapped;
                itemRenderer.OnItemRendererClose -= HandleOnItemRendererClose;

                var pressGesture = itemRenderer.GetComponent<PressGesture>();
                if (pressGesture != null) pressGesture.Pressed -= pressedHandler;
            }
        }

        #region Handlers

        private void HandleOnItemRendererTapped(ItemRenderer itemRenderer)
        {
            if (SelectedItem != itemRenderer)
            {
                SelectedItem = itemRenderer;
                dispatchOnItemSelected(itemRenderer);
            }
            else
            {
                SelectedItem = null;
            }
        }

        private void HandleOnItemRendererClose(ItemRenderer itemRenderer)
        {
            removeItemRenderer(itemRenderer, true);
        }
        
        private void pressedHandler(object sender, System.EventArgs e)
        {
            var child = (sender as Gesture).transform;
            child.SetAsLastSibling();
        }

        #endregion

        #region Layouts

        /**
         * SearchLayoutsForType
         */
        private Layout SearchLayoutsForType(LayoutType type)
        {
            foreach (var layout in Layouts)
            {
                if (layout.LayoutType == type)
                    return layout;
            }

            return null;
        }

        /**
         * updateLayouts
         */
        private void updateLayouts()
        {
            ToggleLayoutsEnabled();
            ApplyCurrentLayout();
        }

        private void ToggleLayoutsEnabled()
        {
            LayoutType selected = Layout;

            foreach (var layout in Layouts)
            {
                bool isSelected = layout.LayoutType == selected;
                layout.enabled = isSelected;
            }
        }

        private void ApplyCurrentLayout()
        {
            Layout layout = CurrentLayout;

            if (layout != null)
                layout.ApplyLayout();
        }

        /**
         * CurrentLayout
         */
        private Layout CurrentLayout
        {
            get
            {
                LayoutType selected = Layout;

                foreach (var layout in Layouts)
                {
                    if (layout.LayoutType == selected)
                        return layout;
                }

                return null;
            }
        }
        
        #endregion

        #region Effects

        /**
         * multiply the start delay by the index to make them appear sequentially but still respect the relative delay
         */
        private void AdjustEffectIn(ItemRenderer itemRenderer, int index)
        {
            EffectIn effectIn = itemRenderer.GetComponent<EffectIn>();
            
            if (effectIn)
                effectIn.StartDelay *= index;
        }

        #endregion

        #region Event Dispatching

        /**
         * dispatchOnItemSelected
         */
        protected void dispatchOnItemSelected(ItemRenderer itemRenderer)
        {
            if (OnItemSelected != null)
                OnItemSelected(itemRenderer);
        }

        /**
         * dispatchOnItemSelected
         */
        protected void dispatchOnContainerEmpty()
        {
            if (OnContainerEmpty != null)
                OnContainerEmpty();
        }

        #endregion
    }
}