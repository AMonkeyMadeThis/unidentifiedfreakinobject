﻿namespace AMonkeyMadeThis.CognitiveServices.Vision.Analyze.Model.VO
{
	using Common.Model.VO;
	using System;

	[Serializable]
	/**
	 * TagVO
	 */
	public class TagVO : ValueObject
	{
		public string name;
		public float confidence;
	}
}
