﻿namespace AMonkeyMadeThis.UnityFlex.UI.Component.Data.Layouts
{
    using UnityEngine;

    /**
     * RingLayout
     */
    public class RingLayout : Layout
    {
        /**
         * ArcAngle
         */
        public float ArcAngle = 360f;

        /**
         * ApplyLayout
         */
        public override void ApplyLayout()
        {
            int numChildren = transform.childCount;

            if (numChildren > 0)
            {
                for (int i = numChildren - 1; i >= 0; i--)
                {
                    int childIndex = numChildren - i;
                    Transform child = transform.GetChild(i);

                    if (child)
                        AlignChild(child, childIndex);
                }
            }
        }

        /**
         * AlignChild
         */
        protected override void AlignChild(Transform child, int index)
        {
            int numItems = DataGroup.dataSource.Length + 1;
            float stepAngle = ArcAngle / (float)numItems;

            Vector3 position = new Vector3();
            position.x = Mathf.Sin(stepAngle * (index+1) * Mathf.Deg2Rad) * DataGroup.Width/2;
            position.y = Mathf.Cos(stepAngle * (index+1) * Mathf.Deg2Rad) * DataGroup.Height/2;

            child.localScale = Vector3.one;
            child.localRotation = Quaternion.Euler(0f, 0f, 0f);
            child.localPosition = position;
        }
    }
}
