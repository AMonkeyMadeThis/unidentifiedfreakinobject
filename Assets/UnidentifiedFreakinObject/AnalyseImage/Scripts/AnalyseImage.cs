﻿namespace UnidentifiedFreakinObject.AnalyseImage.Scripts
{
	using AMonkeyMadeThis.CognitiveServices.Vision.Analyze.Model;
	using AMonkeyMadeThis.CognitiveServices.Vision.Analyze.Service;
	using AMonkeyMadeThis.Common.Model.VO;
	using AMonkeyMadeThis.GoogleAnalytics.Controller;
	using AMonkeyMadeThis.UnityFlex.Network;
	using AMonkeyMadeThis.UnityFlex.UI.Component.Navigation;
	using Model;
	using Shared.Model;
	using Shared.Scripts;
	using System.Collections.Generic;
	using UnityEngine;

	/**
	 * AnalyseImage
	 */
	public class AnalyseImage : AbstractView
	{
		public ViewStack ViewStack;

		private AnalysisResultView AnalysisResultView;
		private AnalysisResultViewModel AnalysisResultViewModel;

		public override void OnEnable()
		{
			base.OnEnable();

			GoToBlurb();
			DisableNextButton();
		}

		protected override void CollectComponents()
		{
			base.CollectComponents();

			AnalysisResultView = GetComponentInChildren<AnalysisResultView>();
			AnalysisResultViewModel = GetComponentInChildren<AnalysisResultViewModel>();
		}

		private void UploadImageForRecognition()
		{
			GoToProcessing();

			Texture2D image = CameraModel.Instance.CroppedImage;

			Responder responder = new Responder(HandleAnalysisResult, HandleAnalysisFailure);

			AnalyzeService.Instance.Post(image, responder);
		}

		protected override void HandleAdvertFinished()
		{
			base.HandleAdvertFinished();

			UploadImageForRecognition();
		}

		protected override void HandleAdvertSkipped()
		{
			base.HandleAdvertSkipped();

			UploadImageForRecognition();
		}

		protected override void HandleAdvertFailed()
		{
			base.HandleAdvertFailed();

			UploadImageForRecognition();
		}

		private void HandleAnalysisResult(object result)
		{
			Debug.Log("HandleAnalysisResult");

			var _result = result as AnalyseResult;

			if (_result != null)
			{
				AnalysisResultViewModel.dataSource = GetData(_result);

				GoToResults();
			}
			else
			{
				GoToError();
			}
		}

		private ValueObject[] GetData(AnalyseResult source)
		{
			List<ValueObject> data = new List<ValueObject>();
			data.AddRange(source.categories);
			data.AddRange(source.description.captions);
			data.AddRange(source.tags);

			ValueObject[] result = new ValueObject[data.Count];

			data.CopyTo(result, 0);

			return result;
		}

		private void HandleAnalysisFailure(object fault)
		{
			Debug.Log("HandleAnalysisFailure");
			GoToError();
		}

		private void GoToBlurb()
		{
			GoToView("Blurb");
		}

		private void GoToProcessing()
		{
			GoToView("Processing");
		}

		private void GoToResults()
		{
			GoToView("Results");
			EnableNextButton();
		}

		private void GoToError()
		{
			GoToView("Error");
		}

		private void GoToView(string viewName)
		{
			GoogleAnalyticsController.Instance.Tracker.LogScreen(viewName);
			ViewStack.GoToLabel(viewName);
		}
	}
}
