﻿namespace AMonkeyMadeThis.FaceBook.Controller
{
	using Common.Controller;
	using Model;
	using UnityEngine;
	using UnityFlex.UI.Component.Navigation;

	[RequireComponent(typeof(ViewStack))]
	/**
	 * FaceBookViewController
	 */
	public class FaceBookViewController : AbstractController
	{
		private ViewStack viewstack;
		
		protected override void CollectComponents()
		{
			base.CollectComponents();

			viewstack = GetComponent<ViewStack>();
		}

		public override void Start()
		{
			base.Start();

			if (FaceBookModel.Instance.IsInitialised)
				ShowAuthenticationStatus();
		}

		public override void AddListeners()
		{
			base.AddListeners();

			AddModelListeners();
		}

		public override void RemoveListeners()
		{
			base.RemoveListeners();

			RemoveModelListeners();
		}

		private void AddModelListeners()
		{
			FaceBookModel.Initialised += HandleFaceBookInitialised;
			FaceBookModel.LoggedIn += HandleFaceBookLoggedIn;
		}

		private void RemoveModelListeners()
		{
			FaceBookModel.Initialised -= HandleFaceBookInitialised;
			FaceBookModel.LoggedIn -= HandleFaceBookLoggedIn;
		}

		private void HandleFaceBookInitialised()
		{
			InvalidateProperties();
		}

		private void HandleFaceBookLoggedIn(bool IsLoggedIn)
		{
			InvalidateProperties();
		}

		protected override void CommitProperties()
		{
			base.CommitProperties();

			ShowAuthenticationStatus();
		}

		private void ShowAuthenticationStatus()
		{
			if (FaceBookModel.Instance.IsInitialised)
			{
				if (FaceBookModel.Instance.IsLoggedIn)
					viewstack.GoToLabel("Authenticated");
				else
					viewstack.GoToLabel("UnAuthenticated");
			}
		}
	}
}
