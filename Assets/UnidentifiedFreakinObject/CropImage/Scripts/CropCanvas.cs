﻿namespace UnidentifiedFreakinObject.CropImage.Scripts
{
	using AMonkeyMadeThis.UnityFlex.UI.Component;
	using Shared.Model;
	using UnityEngine;

	[RequireComponent(typeof(Renderer))]
	/**
	 * CropCanvas
	 */
	public class CropCanvas : UIComponent
	{
		public override void AddListeners()
		{
			base.AddListeners();

			AddModelListeners();
		}

		public override void RemoveListeners()
		{
			base.RemoveListeners();

			RemoveModelListeners();
		}

		private void AddModelListeners()
		{
			CameraModel.RawImageChanged += HandleRawImageChanged;
		}

		private void RemoveModelListeners()
		{
			CameraModel.RawImageChanged -= HandleRawImageChanged;
		}

		private void HandleRawImageChanged(Texture2D texture)
		{
			InvalidateProperties();
		}

		protected override void CommitProperties()
		{
			base.CommitProperties();

			ApplyTexture();
		}

		private void ApplyTexture()
		{
			GetComponent<Renderer>().material.mainTexture = CameraModel.Instance.RawImage;
		}
	}
}
