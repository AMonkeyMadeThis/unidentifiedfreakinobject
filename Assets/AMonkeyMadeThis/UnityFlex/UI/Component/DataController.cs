﻿namespace AMonkeyMadeThis.UnityFlex.UI.Component
{
	using System.Collections;
	using UnityEngine;

	/**
	 * DataController
	 */
	public class DataController : UIComponent
	{
		#region Events

		/**
		 * DataSourceChangedAction
		 */
		public delegate void DataSourceChangedAction();
		/**
		 * OnDataSourceChanged
		 */
		public event DataSourceChangedAction OnDataSourceChanged;
		/**
		 * SelectedIndexChangedAction
		 */
		public delegate void SelectedIndexChangedAction();
		/**
		 * OnSelectedIndexChanged
		 */
		public event SelectedIndexChangedAction OnSelectedIndexChanged;
		/**
		 * SelectedItemChangedAction
		 */
		public delegate void SelectedItemChangedAction();
		/**
		 * OnSelectedItemChanged
		 */
		public event SelectedIndexChangedAction OnSelectedItemChanged;

		#endregion

		#region Member Vars
		
		private IList _datasource;
		/**
		 * Datasource
		 */
		public virtual IList Datasource
		{
			get { return _datasource; }
			set
			{
				if (_datasource != value)
				{
					_datasource = value;
					DispatchOnDataSourceChanged();
					Reset();
				}
			}
		}

		private int _selectedIndex = -1;
		/**
		 * SelectedIndex
		 */
		public virtual int SelectedIndex
		{
			get { return _selectedIndex; }
			set
			{
				if (_selectedIndex != value)
				{
					_selectedIndex = value;
					DispatchOnSelectedIndexChanged();
					UpdateSelectedItem();
				}
			}
		}

		private object _selectedItem;
		/**
		 * SelectedIndex
		 */
		public virtual object SelectedItem
		{
			get { return _selectedItem; }
			set
			{
				if (_selectedItem != value)
				{
					_selectedItem = value;
					DispatchOnSelectedItemChanged();
					UpdateSelectedIndex();
				}
			}
		}

		#endregion

		#region Datasource sync methods
		
		/**
		 * Reset
		 */
		protected virtual void Reset()
		{
			SelectedIndex = -1;
			SelectedItem = null;
		}

		/**
		 * UpdateSelectedItem
		 */
		private void UpdateSelectedItem()
		{
			Debug.Log(this + " UpdateSelectedItem");

			object item = (SelectedIndex>-1 && SelectedIndex<Datasource.Count) ? Datasource[SelectedIndex] : null;
			SelectedItem = item;
		}

		/**
		 * UpdateSelectedIndex
		 */
		protected virtual void UpdateSelectedIndex()
		{
			Debug.Log(this + " UpdateSelectedItem");

			int index = Datasource.IndexOf(SelectedItem);
			SelectedIndex = index;
		}

		#endregion

		#region Event Dispatching

		/**
		 * DispatchOnDataSourceChanged
		 */
		private void DispatchOnDataSourceChanged()
		{
			if (OnDataSourceChanged != null)
				OnDataSourceChanged();
		}

		/**
		 * DispatchOnSelectedIndexChanged
		 */
		private void DispatchOnSelectedIndexChanged()
		{
			if (OnSelectedIndexChanged != null)
				OnSelectedIndexChanged();
		}

		/**
		 * DispatchOnSelectedItemChanged
		 */
		private void DispatchOnSelectedItemChanged()
		{
			if (OnSelectedItemChanged != null)
				OnSelectedItemChanged();
		}

		#endregion
	}
}
