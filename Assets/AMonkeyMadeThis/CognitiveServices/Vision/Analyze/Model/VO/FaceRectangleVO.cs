﻿namespace AMonkeyMadeThis.CognitiveServices.Vision.Analyze.Model.VO
{
	using Common.Model.VO;
	using System;

	[Serializable]
	/**
	 * FaceRectangleVO
	 */
	public class FaceRectangleVO : ValueObject
	{
		public float left;
		public float top;
		public int width;
		public int height;
	}
}
