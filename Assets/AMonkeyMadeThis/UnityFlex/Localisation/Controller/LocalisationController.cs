﻿namespace AMonkeyMadeThis.UnityFlex.Localisation.Controller
{
	using Common.Controller;
	using Enum;
	using Model;
	using System;
	using System.Collections.Generic;
	using UnityEngine;

	/**
	 * LocalisationController
	 */
	public class LocalisationController : AbstractController
	{
		#region Singleton

		private static LocalisationController _instance;

		/**
		 * Instance
		 */
		public static LocalisationController Instance
		{
			get { return _instance; }
			private set
			{
				_instance = value;
			}
		}

		#endregion

		#region Events

		/**
		 * LocalisationAvailableAction
		 */
		public delegate void LocalisationAvailableAction();
		/**
		 * OnLocalisationAvailable
		 */
		public static event LocalisationAvailableAction OnLocalisationAvailable;
		/**
		 * DispatchOnLocalisationAvailable
		 */
		private void DispatchOnLocalisationAvailable()
		{
			if (OnLocalisationAvailable != null)
				OnLocalisationAvailable();
		}
		/**
		 * LocaleChangedAction
		 */
		public delegate void LocaleChangedAction();
		/**
		 * OnLocaleChanged
		 */
		public static event LocaleChangedAction OnLocaleChanged;
		/**
		 * DispatchOnLocaleChanged
		 */
		private void DispatchOnLocaleChanged()
		{
			if (OnLocaleChanged != null)
				OnLocaleChanged();
		}

		#endregion

		#region Instance vars

		private LocaleCode _current;
		/**
		 * Current
		 */
		public LocaleCode Current
		{
			get { return _current; }
			set
			{
				_current = value;
				ChangeLocale();
			}
		}

		/**
		 * Available
		 */
		public List<LocaleChain> Available;
		/**
		 * Chains
		 */
		public LocaleChain[] Chains;
		/**
		 * CurrentChain
		 */
		private LocaleChain CurrentChain
		{
			get
			{
				foreach (var chain in Chains)
				{
					if (chain.Locale == Current)
						return chain;
				}

				return null;
			}
		}
		/**
		 * CurrentChainList
		 */
		private List<LocaleCode> CurrentChainList
		{
			get
			{
				List<LocaleCode> locales = new List<LocaleCode>();
				locales.Add(Current);

				if (CurrentChain!= null && CurrentChain.Chain != null)
					locales.AddRange(CurrentChain.Chain);

				locales.Reverse();

				return locales;
			}
		}

		/**
		 * BundlePath
		 */
		public string BundlePath = "Localisation/Locale/{0}";

		private Dictionary<string, int> _resourceBundleRegistrations;
		/**
		 * ResourceBundleRegistrations
		 */
		public Dictionary<string, int> ResourceBundleRegistrations
		{
			get { return _resourceBundleRegistrations; }
			set
			{
				_resourceBundleRegistrations = value;
			}
		}
		/**
		 * ResourceBundles
		 */
		public Dictionary<string, Dictionary<string, string>> ResourceBundles = new Dictionary<string, Dictionary<string, string>>();

		#endregion

		#region Constructor

		/**
		 * LocalisationController
		 */
		public LocalisationController()
		{
			Instance = this;
			ResourceBundleRegistrations = new Dictionary<string, int>();
			ResourceBundles = new Dictionary<string, Dictionary<string, string>>();
		}

		#endregion

		#region Lifecycle

		/**
		 * Awake
		 */
		public override void Awake()
		{
			base.Awake();

			BuildAvailableLocales();
			SelectInitialLocale();
			DispatchOnLocalisationAvailable();
		}

		#endregion

		/**
		 * CollectComponents
		 */
		protected override void CollectComponents()
		{
			base.CollectComponents();

			Chains = GetComponents<LocaleChain>();
		}

		/**
		 * InitParams
		 */
		private void InitParams()
		{
			//ResourceBundleRegistrations = new Dictionary<string, int>();
			//ResourceBundles = new Dictionary<string, Dictionary<string, string>>();
		}

		private void SelectInitialLocale()
		{
			if (Available.Count > 0)
			{
				Current = Available[0].Locale;
			}
		}

		/**
		 * BuildAvailableLocales
		 */
		private void BuildAvailableLocales()
		{
			Available = new List<LocaleChain>();

			foreach(var link in Chains)
			{
				if (!Available.Contains(link))
					Available.Add(link);
			}
		}

		private void ChangeLocale()
		{
			ResourceBundles.Clear();
			ProcessResourceBundleChain();
			DispatchOnLocaleChanged();
		}

		private void ProcessResourceBundleChain()
		{
			foreach (var locale in CurrentChainList)
			{
				foreach (var registration in ResourceBundleRegistrations)
				{
					string resourceBundleName = registration.Key;

					if (resourceBundleName != null && resourceBundleName.Length > 0)
						BuildResourceBundle(resourceBundleName, locale);
				}
			}
		}

		private void BuildResourceBundle(string resourceBundleName, LocaleCode locale)
		{
			bool resourceBundleExists = ResourceBundles.ContainsKey(resourceBundleName);
			Dictionary<string, string> resourceBundle = (resourceBundleExists) ? ResourceBundles[resourceBundleName] : new Dictionary<string, string>();
			string path = string.Format(BundlePath, locale);
			string file = path + "/" + resourceBundleName;
			var source = Resources.Load<TextAsset>(file);

			if (source != null && source.text.Length > 0)
			{
				string[] lines = source.text.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);

				foreach (var line in lines)
				{
					ProcessLine(resourceBundle, line);
				}

				if (!resourceBundleExists)
					ResourceBundles.Add(resourceBundleName, resourceBundle);
			}
		}

		private void ProcessLine(Dictionary<string, string> resourceBundle, string line)
		{
			if (line.Contains("="))
			{
				string[] keyValuePair = line.Split('=');

				if (keyValuePair != null && keyValuePair.Length == 2)
				{
					string key = keyValuePair[0];
					string value = keyValuePair[1];

					if (resourceBundle.ContainsKey(key))
					{
						resourceBundle[key] = value;
					}
					else
					{
						resourceBundle.Add(key, value);
					}
				}
			}
		}

		/**
		 * RegisterResourceBundleUse
		 */
		public void RegisterResourceBundleUse(string resourceBundleName)
		{
			if (!ResourceBundleRegistrations.ContainsKey(resourceBundleName))
			{
				ResourceBundleRegistrations.Add(resourceBundleName, 1);
				ProcessResourceBundleChain();
			}
			else
			{
				ResourceBundleRegistrations[resourceBundleName]++;
			}
		}

		/**
		 * UnRegisterResourceBundleUse
		 */
		public void UnRegisterResourceBundleUse(string resourceBundleName)
		{
			if (ResourceBundleRegistrations.ContainsKey(resourceBundleName))
			{
				ResourceBundleRegistrations[resourceBundleName]--;

				if (ResourceBundleRegistrations[resourceBundleName] <= 0)
					ResourceBundleRegistrations.Remove(resourceBundleName);
			}
		}

		public string LocaliseString(string key, string resourceBundleName = "Application")
		{
			string _resourceBundleName = (resourceBundleName != null && resourceBundleName.Length > 0) ? resourceBundleName : "Application";
			
			if (_resourceBundleName != null && _resourceBundleName.Length > 0 && ResourceBundles.ContainsKey(_resourceBundleName))
			{
				Dictionary<string, string> resourceBundle = ResourceBundles[_resourceBundleName];

				if (resourceBundle != null && key != null && key.Length > 0 && resourceBundle.ContainsKey(key))
				{
					string _result = resourceBundle[key];
					
					return _result;
				}
			}
			
			return string.Empty;
		}
	}
}
