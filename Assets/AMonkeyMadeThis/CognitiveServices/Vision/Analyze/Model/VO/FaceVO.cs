﻿namespace AMonkeyMadeThis.CognitiveServices.Vision.Analyze.Model.VO
{
	using Common.Model.VO;
	using System;

	[Serializable]
	/**
	 * FaceVO
	 */
	public class FaceVO : ValueObject
	{
		public int age;
		public string gender;
		public FaceRectangleVO faceRectangle;
	}
}
