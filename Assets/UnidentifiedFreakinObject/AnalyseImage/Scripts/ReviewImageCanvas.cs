﻿namespace UnidentifiedFreakinObject.AnalyseImage.Scripts
{
	using AMonkeyMadeThis.Common.Controller;
	using Shared.Model;
	using UnityEngine;
	using UnityEngine.UI;

	[RequireComponent(typeof(Image))]
	/**
	 * ReviewImageCanvas
	 */
	public class ReviewImageCanvas : AbstractController
	{
		public override void AddListeners()
		{
			base.AddListeners();

			AddModelListeners();
		}

		public override void RemoveListeners()
		{
			base.RemoveListeners();

			RemoveModelListeners();
		}

		private void AddModelListeners()
		{
			CameraModel.CroppedImageChanged += HandleCroppedImageChanged;
		}

		private void RemoveModelListeners()
		{
			CameraModel.CroppedImageChanged -= HandleCroppedImageChanged;
		}

		private void HandleCroppedImageChanged(Texture2D texture)
		{
			InvalidateProperties();
		}

		protected override void CommitProperties()
		{
			base.CommitProperties();

			ApplyTexture();
		}

		private void ApplyTexture()
		{
			Texture2D texture = CameraModel.Instance.CroppedImage;
			Rect rect = new Rect(0, 0, texture.width, texture.height);
			Vector2 pivot = Vector2.zero;

			GetComponent<Image>().sprite = Sprite.Create(texture, rect, pivot);
		}
	}
}
