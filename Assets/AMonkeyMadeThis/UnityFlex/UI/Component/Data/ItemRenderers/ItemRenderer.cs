﻿namespace AMonkeyMadeThis.UnityFlex.UI.Component.Data.ItemRenderers
{
	using Common.Model.VO;
	using Component;
	using Effects;
	using System;
	using TouchScript.Gestures;
	using UnityEngine;

	/**
     * ItemRenderer
     */
	public abstract class ItemRenderer : UIComponent
	{
        #region Events

        /**
         * ItemRendererTappedAction
         */
        public delegate void ItemRendererTappedAction(ItemRenderer itemRenderer);
        /**
         * OnItemRendererTapped
         */
        public event ItemRendererTappedAction OnItemRendererTapped;
        /**
         * ItemRendererCloseAction
         */
        public delegate void ItemRendererCloseAction(ItemRenderer itemRenderer);
        /**
         * OnItemRendererClose
         */
        public event ItemRendererCloseAction OnItemRendererClose;

		#endregion
		
		private bool _selected;
        /**
         * Selected
         */
        public bool Selected
        {
            get { return _selected; }
            set
            {
                _selected = value;
                UpdateSelectedState();
            }
        }
        /**
         * Data
         */
        public ValueObject Data { get; set; }
        /**
         * MaximisedScale
         */
        public Vector3 MaximisedScale = new Vector3(6, 6, 6);
        /**
         * isMaximised
         */
        protected bool isMaximised;
        /**
         * originalScale
         */
        private Vector3 originalScale;
        /**
         * originalRotation
         */
        private Vector3 originalRotation;
        /**
         * originalPosition
         */
        private Vector3 originalPosition;
        /**
         * TapGesture
         */
        private TapGesture TapGesture;
        /**
         * EffectDynamic
         */
        private EffectDynamic EffectDynamic;

		public override void Start()
		{
			base.Start();

			InvalidateProperties();
		}

		protected override void CommitProperties()
		{
			base.CommitProperties();

			PopulateUI();
		}

		protected virtual void PopulateUI()
		{
			//template method
		}

		/**
         * CollectComponents
         */
		protected override void CollectComponents()
        {
            TapGesture = GetComponent<TapGesture>();
            EffectDynamic = GetComponent<EffectDynamic>();
        }
        
        /**
         * ToggleMaximised
         */
        public void UpdateSelectedState()
        {
            if (Selected && !isMaximised)
            {
                transform.SetAsLastSibling();
                StoreOriginalSize();
                Maximise();
                isMaximised = true;
            }
            else if (!Selected && isMaximised)
            {
                RestoreOriginalSize();
                isMaximised = false;
            }
        }

        /**
         * ToggleMaximised
         */
        public void ToggleMaximised()
        {
            if (isMaximised)
            {
                RestoreOriginalSize();
                isMaximised = false;
            }
            else
            {
                StoreOriginalSize();
                Maximise();
                isMaximised = true;
            }
        }

        private void RestoreOriginalSize()
        {
            if (EffectDynamic)
            {
                EffectDynamic.StartPosition = this.transform.localPosition;
                EffectDynamic.StartRotation = this.transform.localRotation.eulerAngles;
                EffectDynamic.StartScale = this.transform.localScale;

                EffectDynamic.EndPosition = originalPosition;
                EffectDynamic.EndRotation = originalRotation;
                EffectDynamic.EndScale = originalScale;

                EffectDynamic.Play();
            }
        }

        private void Maximise()
        {
            if (EffectDynamic)
            {
                EffectDynamic.StartPosition = this.transform.localPosition;
                EffectDynamic.StartRotation = this.transform.localRotation.eulerAngles;
                EffectDynamic.StartScale = this.transform.localScale;

                EffectDynamic.EndPosition = Vector3.zero;
                EffectDynamic.EndRotation = Vector3.zero;
                EffectDynamic.EndScale = MaximisedScale;

                EffectDynamic.Play();
            }
        }

        private void StoreOriginalSize()
        {
            originalScale = this.transform.localScale;
            originalPosition = this.transform.localPosition;
            originalRotation = this.transform.localRotation.eulerAngles;
        }

        public override void AddListeners()
        {
            base.AddListeners();
            AddGestureListeners();
        }

        private void AddGestureListeners()
        {
            if (TapGesture != null)
                TapGesture.Tapped += HandleTapped;

            if (EffectDynamic != null)
                EffectDynamic.OnEffectEnd += HandleMaximiseMinimiseEffectEnd;
        }

        protected virtual void HandleMaximiseMinimiseEffectEnd()
        {
            // Template method
        }

        public override void RemoveListeners()
        {
            base.RemoveListeners();
            RemoveGestureListeners();
        }

        private void RemoveGestureListeners()
        {
            if (TapGesture != null)
                TapGesture.Tapped -= HandleTapped;

            if (EffectDynamic != null)
                EffectDynamic.OnEffectEnd += HandleMaximiseMinimiseEffectEnd;
        }
        
        private void HandleTapped(object sender, EventArgs e)
        {
            dispatchOnItemRendererTapped();
        }
        
        public void Close()
        {
            EffectOut effectOut = GetComponent<EffectOut>();

            if (effectOut != null)
            {
                effectOut.OnEffectEnd += HandleOnEffectEnd;
                effectOut.Play();
            }
            else
            {
                dispatchOnItemRendererClose();
            }
        }

        protected virtual void HandleOnEffectEnd()
        {
            EffectOut effectOut = GetComponent<EffectOut>();

            if (effectOut != null)
                effectOut.OnEffectEnd -= HandleOnEffectEnd;

            dispatchOnItemRendererClose();
        }

        /**
         * dispatchOnItemRendererTapped
         */
        private void dispatchOnItemRendererTapped()
        {
            if (OnItemRendererTapped != null)
                OnItemRendererTapped(this);
        }

        /**
         * dispatchOnItemRendererClose
         */
        private void dispatchOnItemRendererClose()
        {
            if (OnItemRendererClose != null)
                OnItemRendererClose(this);
        }
    }
}
