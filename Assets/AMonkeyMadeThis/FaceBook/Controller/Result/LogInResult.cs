﻿namespace AMonkeyMadeThis.FaceBook.Controller.Result
{
	using Enum;
	using System;
	using System.Collections.Generic;

	/*
	{
		"permissions":"user_friends,email,public_profile",
		"expiration_timestamp":"1472463535",
		"access_token":"EAAB7tnq9SJkBALEFiNZAWuZBIp4SIjBNoppSoPtIEn9JhLTBZB8mzyVOFPoN3zLjhAjCb2UolWqzCdECrn63dv7KNsPBFBg5k2SryGrXAT9AIbYSUTV2ZAoMHTJBZCpgZBX5tU3ZBupmRtdOwrQEkK026kgV68jEn3J8QMtTzZB5fAZDZD",
		"user_id":"10157145567800650",
		"last_refresh":"1467279535",
		"granted_permissions":["user_friends","email","public_profile"],
		"declined_permissions":["publish_actions"],"
		callback_id":"2"}
	*/

	/**
	 * LogInResult
	 */
	public class LogInResult
	{
		public string user_id;
		public string permissions;
		public List<Permission> granted_permissions;
		public List<Permission> declined_permissions;
		public int callback_id;
		public int last_refresh;
		public string access_token;
		public int expiration_timestamp;
	}
}
