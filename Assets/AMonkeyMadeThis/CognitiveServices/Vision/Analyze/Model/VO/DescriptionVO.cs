﻿namespace AMonkeyMadeThis.CognitiveServices.Vision.Analyze.Model.VO
{
	using Common.Model.VO;
	using System;

	[Serializable]
	/**
	 * DescriptionVO
	 */
	public class DescriptionVO : ValueObject
	{
		public string[] tags;
		public CaptionVO[] captions;
	}
}
