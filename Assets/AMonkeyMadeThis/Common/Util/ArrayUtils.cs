﻿namespace AMonkeyMadeThis.Common.Util
{
	/**
	 * ArrayUtils
	 */
	public static class ArrayUtils
	{
		/**
		 * Add
		 */
		public static T[] Add<T>(this T[] target, T item)
		{
			if (target != null)
			{
				T[] result = new T[target.Length + 1];
				target.CopyTo(result, 0);
				result[target.Length] = item;

				return result;
			}

			return null;
		}
	}
}
