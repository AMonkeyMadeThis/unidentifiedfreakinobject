﻿namespace AMonkeyMadeThis.GoogleAnalytics.Controller
{
	using Common.Controller;

	/**
	 * GoogleAnalyticsController
	 */
	public class GoogleAnalyticsController : AbstractController
	{
		#region Singleton

		private static GoogleAnalyticsController _instance;

		public static GoogleAnalyticsController Instance
		{
			get { return _instance; }
			private set
			{
				_instance = value;
			}
		}

		#endregion

		#region CTOR

		public GoogleAnalyticsController()
		{
			Instance = this;
		}

		#endregion

		#region Member vars

		public GoogleAnalyticsV4 Tracker;

		#endregion
	}
}
