﻿namespace AMonkeyMadeThis.Ads.Controller
{
	using Common.Controller;
	using System.Collections;
	using UnityEngine;
	using UnityEngine.Advertisements;
	using System;
	using GoogleAnalytics.Controller;
	/**
	 * UnityAdsController
	 */
	public class UnityAdsController : AbstractController
	{
		private static UnityAdsController _instance;

		public static UnityAdsController Instance
		{
			get { return _instance; }
			private set
			{
				_instance = value;
			}
		}

		public UnityAdsController()
		{
			Instance = this;
		}

		public void ShowAd(Action<ShowResult> resultCallback, string zone = null)
		{
			GoogleAnalyticsController.Instance.Tracker.LogEvent(new EventHitBuilder().SetEventCategory("Advert").SetEventAction("Start"));

			#if UNITY_EDITOR
				StartCoroutine(WaitForAd());
			#endif
			
			ShowOptions options = new ShowOptions();
			options.resultCallback = resultCallback;

			if (Advertisement.IsReady(zone))
				Advertisement.Show(zone, options);
		}

		void AdCallbackhandler(ShowResult result)
		{
			switch (result)
			{
				case ShowResult.Finished:
					Debug.Log("Ad Finished. Rewarding player...");
					break;
				case ShowResult.Skipped:
					Debug.Log("Ad skipped. Son, I am dissapointed in you");
					break;
				case ShowResult.Failed:
					Debug.Log("I swear this has never happened to me before");
					break;
			}
		}

		IEnumerator WaitForAd()
		{
			float currentTimeScale = Time.timeScale;
			Time.timeScale = 0f;
			yield return null;

			while (Advertisement.isShowing)
				yield return null;

			Time.timeScale = currentTimeScale;
		}
	}
}
