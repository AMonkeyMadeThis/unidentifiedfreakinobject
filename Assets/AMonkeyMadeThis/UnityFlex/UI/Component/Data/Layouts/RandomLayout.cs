﻿namespace AMonkeyMadeThis.UnityFlex.UI.Component.Data.Layouts
{
    using UnityEngine;

    /**
     * RandomLayout
     */
    public class RandomLayout : Layout
    {
        /**
         * AlignChild
         */
        protected override void AlignChild(Transform child, int index)
        {
            child.localScale = Vector3.one;
            child.localRotation = Quaternion.Euler(0f, 0f, Random.Range(0f, 360f));
            child.localPosition = new Vector3(Random.Range(-DataGroup.Width / 4, DataGroup.Width / 4), Random.Range(-DataGroup.Height / 2, DataGroup.Height / 2), 0);
        }
    }
}
