﻿namespace AMonkeyMadeThis.Util
{
	using System.IO;
	using System.Text;

	/**
     * StringUtils
     */
	public class StringUtils
	{
		/**
		 * ConvertStreamToDataString
		 */
		public static string ConvertStreamToDataString(MemoryStream stream)
		{
			int length = (int)stream.Length;
			byte[] byteArray = new byte[length];

			stream.Read(byteArray, 0, length);

			string result = GetStringFromByteArray(byteArray);

			return result;
		}

		/**
         * ConvertDataStringToStream
         */
		public static Stream ConvertDataStringToStream(string raw)
		{
			byte[] byteArray = GetUTF8ByteArrayFromString(raw);
			MemoryStream result = new MemoryStream(byteArray);
			
			return result;
		}

		/**
         * GetUTF8ByteArrayFromString
         * 
         * Gets the UTF8 bytes from a string.
         */
		public static byte[] GetUTF8ByteArrayFromString(string raw)
		{
			UTF8Encoding encoding = new UTF8Encoding();
			byte[] result = encoding.GetBytes(raw);

			return result;
		}

		/**
         * GetStringFromByteArray
         * 
         * Gets the UTF8 string from bytes.
         */
		public static string GetStringFromByteArray(byte[] byteArray)
		{
			string result = Encoding.UTF8.GetString(byteArray);

			return result;
		}
	}
}
