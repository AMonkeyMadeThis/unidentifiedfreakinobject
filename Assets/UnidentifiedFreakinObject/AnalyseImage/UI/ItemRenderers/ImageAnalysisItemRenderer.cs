﻿namespace UnidentifiedFreakinObject.AnalyseImage.UI.ItemRenderers
{
	using AMonkeyMadeThis.Common.Util;
	using AMonkeyMadeThis.UnityFlex.Localisation.Controller;
	using AMonkeyMadeThis.UnityFlex.UI.Component.Data.ItemRenderers;
	using UnityEngine.UI;

	/**
	 * ImageAnalysisItemRenderer
	 */
	public class ImageAnalysisItemRenderer : ItemRenderer
	{
		public Text NameLabel;
		public Text ConfidenceLabel;
		
		protected virtual void UpdateNameLabel(string name)
		{
			if (NameLabel == null)
				return;
			
			NameLabel.text = name;
		}

		protected virtual void PopulateConfidenceLabel(float confidence)
		{
			if (ConfidenceLabel == null)
				return;

			string formattedConfidence = NumberUtils.PercentageFormat(confidence);
			string pattern = LocalisationController.Instance.LocaliseString("CognitiveServices.vision.analyse.result.confidence", "CognitiveServices");
			string formattedString = string.Format(pattern, formattedConfidence);

			ConfidenceLabel.text = formattedString;
		}
	}
}
