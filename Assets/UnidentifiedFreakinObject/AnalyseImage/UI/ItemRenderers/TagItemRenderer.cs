﻿namespace UnidentifiedFreakinObject.AnalyseImage.UI.ItemRenderers
{
	using AMonkeyMadeThis.CognitiveServices.Vision.Analyze.Model.VO;

	/**
	 * TagItemRenderer
	 */
	public class TagItemRenderer : ImageAnalysisItemRenderer
	{
		private TagVO TagData
		{
			get { return Data as TagVO; }
		}
		
		protected override void PopulateUI()
		{
			if (TagData != null)
			{
				UpdateNameLabel(TagData.name);
				PopulateConfidenceLabel(TagData.confidence);
			}
		}
	}
}
