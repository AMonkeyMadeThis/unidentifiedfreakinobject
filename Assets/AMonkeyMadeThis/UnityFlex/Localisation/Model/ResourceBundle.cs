﻿namespace AMonkeyMadeThis.UnityFlex.Localisation.Model
{
	using System.Collections.Generic;

	/**
	 * ResourceBundle
	 */
	public class ResourceBundle
	{
		/**
		 * Name
		 */
		public string BundleName;
		/**
		 * Content
		 */
		public Dictionary<string, object> Content;
	}
}