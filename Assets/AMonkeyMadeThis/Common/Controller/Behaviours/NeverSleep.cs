﻿namespace AMonkeyMadeThis.Common.Controller.Behaviours
{
	using UnityEngine;

	/**
	 * NeverSleep
	 */
	public class NeverSleep : AbstractController
	{
		public override void Awake()
		{
			base.Awake();

			Screen.sleepTimeout = SleepTimeout.NeverSleep;
		}
	}
}
