﻿namespace UnidentifiedFreakinObject.Shared.Model
{
	using System;
	using AMonkeyMadeThis.Common.Model;
	using UnityEngine;

	/**
	 * CameraModel
	 */
	public class CameraModel : AbstractModel
	{
		#region Singleton

		private static CameraModel _instance;

		public static CameraModel Instance
		{
			get { return _instance; }
			private set
			{
				_instance = value;
			}
		}

		#endregion

		#region Events
		
		public delegate void CameraDeviceNameChangedAction(string deviceName);
		public static event CameraDeviceNameChangedAction CameraDeviceNameChanged;

		public delegate void RawImageChangedAction(Texture2D texture);
		public static event RawImageChangedAction RawImageChanged;

		public delegate void CroppedImageChangedAction(Texture2D texture);
		public static event CroppedImageChangedAction CroppedImageChanged;

		#endregion

		#region CTOR

		public CameraModel()
		{
			Instance = this;
		}

		#endregion

		#region Member Vars

		[SerializeField]
		private string _deviceName;

		public string DeviceName
		{
			get { return _deviceName; }
			set
			{
				if (_deviceName != value)
				{
					_deviceName = value;
					DispatchDeviceNameChanged();
				}
			}
		}

		private Texture2D _rawImage;

		public Texture2D RawImage
		{
			get { return _rawImage; }
			set
			{
				if (_rawImage != value)
				{
					_rawImage = value;
					DispatchRawImageChanged();
				}
			}
		}

		private Texture2D _croppedImage;

		public Texture2D CroppedImage
		{
			get { return _croppedImage; }
			set
			{
				if (_croppedImage != value)
				{
					_croppedImage = value;
					DispatchCroppedImageChanged();
				}
			}
		}

		#endregion

		#region Lifecycle

		public void OnEnable()
		{
			DeviceName = DefaultWebCamName();
		}

		public void OnDisable()
		{

		}

		#endregion

		#region Defaults
		
		private string DefaultWebCamName()
		{
			if (WebCamTexture.devices != null && WebCamTexture.devices.Length > 0)
				return WebCamTexture.devices[0].name;
			else
				return null;
		}

		#endregion

		#region Event Dispatcher

		private void DispatchDeviceNameChanged()
		{
			if (CameraDeviceNameChanged != null)
				CameraDeviceNameChanged(DeviceName);
		}

		private void DispatchRawImageChanged()
		{
			if (RawImageChanged != null)
				RawImageChanged(RawImage);
		}

		private void DispatchCroppedImageChanged()
		{
			if (CroppedImageChanged != null)
				CroppedImageChanged(CroppedImage);
		}

		#endregion
	}
}
