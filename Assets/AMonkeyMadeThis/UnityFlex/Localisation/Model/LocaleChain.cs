﻿namespace AMonkeyMadeThis.UnityFlex.Localisation.Model
{
	using Enum;
	using UnityEngine;

	/**
	 * LocaleChain
	 */
	public class LocaleChain : MonoBehaviour
	{
		/**
		 * Locale
		 */
		public LocaleCode Locale;
		/**
		 * Label
		 */
		public string Label;
		/**
		 * Flag
		 */
		public Sprite Flag;
		/**
		 * Chain
		 */
		public LocaleCode[] Chain;
	}
}
