﻿namespace AMonkeyMadeThis.CognitiveServices.Vision.Analyze.Enum
{
	using System;

	[Flags]
	/**
	 * VisualFeatures
	 */
	public enum VisualFeatures
	{
		Categories,
		Tags,
		Description,
		Faces
	}
}
