﻿namespace UnidentifiedFreakinObject.Main.Scripts
{
	using AMonkeyMadeThis.FaceBook.Model;
	using Shared.Scripts;

	/**
	 * Main
	 */
	public class Main : AbstractView
	{
		public override void AddListeners()
		{
			base.AddListeners();
			
			AddModelListeners();
		}

		public override void RemoveListeners()
		{
			base.RemoveListeners();

			RemoveModelListeners();
		}

		private void AddModelListeners()
		{
			FaceBookModel.Initialised += HandleFaceBookInitialised;
		}

		private void RemoveModelListeners()
		{
			FaceBookModel.Initialised -= HandleFaceBookInitialised;
		}

		private void HandleFaceBookInitialised()
		{
			GoToHome();
		}
	}
}
