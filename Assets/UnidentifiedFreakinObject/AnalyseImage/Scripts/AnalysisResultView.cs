﻿namespace UnidentifiedFreakinObject.AnalyseImage.Scripts
{
	using AMonkeyMadeThis.CognitiveServices.Vision.Analyze.Model.VO;
	using AMonkeyMadeThis.Common.Model.VO;
	using AMonkeyMadeThis.UnityFlex.UI.Component.Content;
	using AMonkeyMadeThis.UnityFlex.UI.Component.Data.ItemRenderers;
	using UnidentifiedFreakinObject.AnalyseImage.Model;
	using UnidentifiedFreakinObject.AnalyseImage.UI.ItemRenderers;
	using UnityEngine;

	[RequireComponent(typeof(AnalysisResultViewModel))]
	/**
	 * AnalysisResultView
	 */
	public class AnalysisResultView : Page
	{
		#region Member vars

		protected AnalysisResultViewModel Model;

		#endregion

		#region Lifecycle

		protected override void CollectComponents()
		{
			base.CollectComponents();

			Model = GetComponent<AnalysisResultViewModel>();
		}

		public override void OnEnable()
		{
			base.OnEnable();

			this.dataSource = Model.dataSource;
		}
		
		/**
         * OnDisable
         */
		public override void OnDisable()
		{
			RemoveListeners();

			container.dataSource = null;
		}

		#endregion

		#region ItemRenderers

		/**
         * ItemRendererFunction allows a DataGroup to delegate its requests for an item renderer
         * so that the components controlling it are able to configure the ItemRenderers used
         * to display the contents of the datasource.
         * 
         * However, as unity uses prefabs and we can't just instantiate a script we need to hold
         * an array of the prefb'd item renderers.
         */
		protected override ItemRenderer ItemRendererFunction(ValueObject item)
		{
            if (item is CategoryVO)
			{
				return SearchItemRenderersForType(typeof(CategoryItemRenderer));
			}
			else if (item is CaptionVO)
            {
                return SearchItemRenderersForType(typeof(CaptionItemRenderer));
            }
			else if (item is TagVO)
			{
				return SearchItemRenderersForType(typeof(TagItemRenderer));
			}

			return null;
		}

		#endregion
	}
}
