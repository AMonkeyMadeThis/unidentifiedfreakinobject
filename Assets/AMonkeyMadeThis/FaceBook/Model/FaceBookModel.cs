﻿namespace AMonkeyMadeThis.FaceBook.Model
{
	using VO;
	using Common.Model;
	using UnityEngine;

	/**
	 * FaceBookModel
	 */
	public class FaceBookModel : AbstractModel
	{
		#region Singleton

		private static FaceBookModel _instance;

		public static FaceBookModel Instance
		{
			get { return _instance; }
			private set
			{
				_instance = value;
			}
		}

		#endregion

		#region CTOR

		public FaceBookModel()
		{
			Instance = this;
		}

		#endregion

		#region Events

		public delegate void FaceBookInitialisedAction();
		public static event FaceBookInitialisedAction Initialised;

		public delegate void FaceBookLoggedInAction(bool isLoggedIn);
		public static event FaceBookLoggedInAction LoggedIn;

		public delegate void FaceBookUserChangedAction(User user);
		public static event FaceBookUserChangedAction UserChanged;

		#endregion

		private bool _isInitialised;

		public bool IsInitialised
		{
			get { return _isInitialised; }
			set
			{
				if (_isInitialised != value)
				{
					_isInitialised = value;
					DispatchInitialised();
				}
			}
		}

		private bool _isLoggedIn;

		public bool IsLoggedIn
		{
			get { return _isLoggedIn; }
			set
			{
				if (_isLoggedIn != value)
				{
					_isLoggedIn = value;
					DispatchIsLoggedIn();
				}
			}
		}

		private User _user;

		public User User
		{
			get { return _user; }
			set
			{
				if (_user != value)
				{
					_user = value;
					DispatchUserChange();
					IsLoggedIn = User != null;
				}
			}
		}

		private void DispatchInitialised()
		{
			if (Initialised != null)
				Initialised();
		}

		private void DispatchIsLoggedIn()
		{
			if (LoggedIn != null)
				LoggedIn(IsLoggedIn);
		}

		private void DispatchUserChange()
		{
			if (UserChanged != null)
				UserChanged(User);
		}
	}
}
