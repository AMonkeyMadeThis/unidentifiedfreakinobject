﻿namespace AMonkeyMadeThis.UnityFlex.UI.Component.Data.Layouts
{
    using System;
    using UnityEngine;

    [Flags]
    /**
     * StackHorizontalAlign
     */
    public enum StackHorizontalAlign
    {
        Left,
        Centre,
        Right
    }

    [Flags]
    /**
     * StackVerticalAlign
     */
    public enum StackVerticalAlign
    {
        Top,
        Middle,
        Bottom
    }

    /**
     * StackLayout
     */
    public class StackLayout : Layout
    {
        /**
         * VerticalAlign
         */
        public StackVerticalAlign VerticalAlign = StackVerticalAlign.Top;
        /**
         * HorizontalAlign
         */
        public StackHorizontalAlign HorizontalAlign = StackHorizontalAlign.Left;
        /**
         * Scale
         */
        public float Scale = .75f;

        /**
         * ApplyLayout
         */
        public override void ApplyLayout()
        {
            int numChildren = transform.childCount;

            if (numChildren>0)
            {
                for (int i = numChildren-1; i >= 0; i--)
                {
                    int childIndex = numChildren - i;
                    Transform child = transform.GetChild(i);

                    if (child)
                        AlignChild(child, childIndex);
                }
            }
        }

        /**
         * AlignChild
         */
        protected override void AlignChild(Transform child, int index)
        {
            int numItems = DataGroup.dataSource.Length;
            float columnWidth = (2 * DataGroup.Width * Scale) / numItems;
            float rowHeight = (DataGroup.Height * Scale) / numItems;

            Vector3 position = new Vector3();
            position.x = (SignPosX * (DataGroup.Width * Scale) / 1) + (columnWidth * index * SignDirectionX);
            position.y = (SignPosY * (DataGroup.Height * Scale) / 2) + (rowHeight * index * SignDirectionY);

            child.localScale = Vector3.one;
            child.localRotation = Quaternion.Euler(0f, 0f, 0f);
            child.localPosition = position;
        }

        private int SignPosX
        {
            get
            {
                if (HorizontalAlign == StackHorizontalAlign.Left)
                {
                    return -1;
                }
                else if (HorizontalAlign == StackHorizontalAlign.Right)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
        }

        private int SignPosY
        {
            get
            {
                if (VerticalAlign == StackVerticalAlign.Top)
                {
                    return 1;
                }
                else if (VerticalAlign == StackVerticalAlign.Bottom)
                {
                    return -1;
                }
                else
                {
                    return 0;
                }
            }
        }

        private int SignDirectionX
        {
            get
            {
                return SignPosX * -1;
            }
        }

        private int SignDirectionY
        {
            get
            {
                return SignPosY * -1;
            }
        }
    }
}
