﻿namespace AMonkeyMadeThis.UnityFlex.UI.Effects
{
    using System.Collections;
    using UnityEngine;

    /**
     * EffectDynamic
     */
    public class EffectDynamic : MonoBehaviour
    {
        #region Events

        /**
         * EffectEndAction
         */
        public delegate void EffectEndAction();
        /**
         * OnEffectEnd
         */
        public event EffectEndAction OnEffectEnd;

        #endregion

        /**
         * StartPosition
         */
        public Vector3 StartPosition;
        /**
         * StartRotation
         */
        public Vector3 StartRotation;
        /**
         * StartScale
         */
        public Vector3 StartScale;
        /**
         * EndPosition
         */
        public Vector3 EndPosition;
        /**
         * EndRotation
         */
        public Vector3 EndRotation;
        /**
         * EndScale
         */
        public Vector3 EndScale;

        /**
         * StartDelay
         */
        public float StartDelay = 0f;
        /**
         * Duration
         */
        public float Duration = 0f;
        
        /**
         * Play
         */
        public void Play()
        {
            StartCoroutine("Animate");
        }
        
        private IEnumerator Animate()
        {
            UpdateTransform(StartPosition, StartRotation, StartScale);

            yield return new WaitForSeconds(StartDelay);

            double startTime = Time.realtimeSinceStartup;
            double endTime = startTime + Duration;

            while (Time.realtimeSinceStartup < endTime)
            {
                float progress = (float)((Time.realtimeSinceStartup-startTime) / Duration);

                Vector3 position = Vector3.Lerp(StartPosition, EndPosition, progress);
                Vector3 rotation = Vector3.Lerp(StartRotation, EndRotation, progress);
                Vector3 scale = Vector3.Lerp(StartScale, EndScale, progress);
                
                UpdateTransform(position, rotation, scale);

                yield return new WaitForEndOfFrame();
            }

            UpdateTransform(EndPosition, EndRotation, EndScale);

            DispatchEffectEnd();
        }

        private void UpdateTransform(Vector3 position, Vector3 rotation, Vector3 scale)
        {
            // position
            transform.localPosition = position;
            // rotation
            Quaternion _rotation = new Quaternion();
            _rotation.eulerAngles = rotation;
            transform.localRotation = _rotation;
            // scale
            transform.localScale = scale;
        }

        /**
         * DispatchEffectEnd
         */
        protected void DispatchEffectEnd()
        {
            if (OnEffectEnd != null)
                OnEffectEnd();
        }
    }
}
