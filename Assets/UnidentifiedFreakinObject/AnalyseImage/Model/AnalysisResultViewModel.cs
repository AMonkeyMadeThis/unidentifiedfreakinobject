﻿namespace UnidentifiedFreakinObject.AnalyseImage.Model
{
	using AMonkeyMadeThis.Common.Model;
	using AMonkeyMadeThis.Common.Model.VO;

	/**
	 * AnalysisResultViewModel
	 */
	public class AnalysisResultViewModel : AbstractModel
	{
		#region Vars

		private ValueObject[] _dataSource;
		/**
		 * dataSource
		 */
		public ValueObject[] dataSource
		{
			get { return _dataSource; }
			set
			{
				_dataSource = value;
			}
		}

		#endregion
	}
}
