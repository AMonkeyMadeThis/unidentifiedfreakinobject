﻿namespace AMonkeyMadeThis.FaceBook.Controller
{
	using Result;
	using Common.Controller;
	using Enum;
	using Facebook.Unity;
	using Model;
	using System.Collections.Generic;
	using UnityEngine;
	using Model.VO;
	using System;
	using Facebook.MiniJSON;
	/**
	 * FacebookController
	 */
	public class FaceBookController : AbstractController
	{
		#region Singleton

		private static FaceBookController _instance;

		public static FaceBookController Instance
		{
			get { return _instance; }
			private set
			{
				_instance = value;
			}
		}

		#endregion

		#region CTOR

		public FaceBookController()
		{
			Instance = this;
		}

		#endregion

		#region Events
		
		public delegate void FaceBookVisibilityChangeAction(bool isVisible);
		public static event FaceBookVisibilityChangeAction VisibilityChanged;

		#endregion

		#region Vars

		public Permission[] BasicPermissions;

		public float InitialiseDelay = 1f;

		#endregion

		#region Lifecycle

		public override void Start()
		{
			base.Start();

			Invoke("Initialise", InitialiseDelay);
		}

		#endregion

		#region Initialise

		public void Initialise()
		{
			Debug.Log("Initialise " + FB.IsInitialized);

			if (!FB.IsInitialized)
				FB.Init(HandleFBInit, HandleFBHide);
		}

		#endregion

		#region Authentication

		public void LogIn()
		{
			IEnumerable<string> permissions = new List<string>(BasicPermissions.ToString().Split(','));

			FB.LogInWithReadPermissions(permissions, this.HandleLogInResult);
		}

		private void HandleLogInResult(ILoginResult result)
		{
			if (string.IsNullOrEmpty(result.Error) && !result.Cancelled)
				LoadUser();
		}

		public void LogOut()
		{
			FB.LogOut();
			FaceBookModel.Instance.User = null;
		}

		#endregion

		void LoadUser()
		{
			FB.API("me?fields=id,first_name", HttpMethod.GET, this.HandleLoadUserResult);
		}

		private void HandleLoadUserResult(IGraphResult result)
		{
			if (string.IsNullOrEmpty(result.Error) && !result.Cancelled)
			{
				var dict = Json.Deserialize(result.RawResult) as Dictionary<string, object>; //.FromJson<LogInResult>(result.RawResult);

				User user = new User();
				user.ID = (string)dict["id"];
				user.FirstName = (string)dict["first_name"];
				//user.Permissions = Enum.Parse(typeof(Permission), (Array)dict["granted_permissions"].ToString());

				FaceBookModel.Instance.User = user;
			}
		}

		#region Handlers

		protected void HandleFBInit()
		{
			string logMessage = string.Format(
				"HandleFBInit IsLoggedIn='{0}' IsInitialized='{1}'",
				FB.IsLoggedIn,
				FB.IsInitialized);

			Debug.Log(logMessage);

			FaceBookModel.Instance.IsInitialised = FB.IsInitialized;
			FaceBookModel.Instance.IsLoggedIn = FB.IsLoggedIn;

			if (FaceBookModel.Instance.IsLoggedIn)
				LoadUser();
		}

		protected void HandleFBHide(bool isUnityShown)
		{
			Debug.Log("HandleFBHide " + isUnityShown);

			DispatchVisibilityChange(isUnityShown);
		}

		#endregion

		#region Dispatchers
		
		protected void DispatchVisibilityChange(bool isVisible)
		{
			if (VisibilityChanged != null)
				VisibilityChanged(isVisible);
		}

		#endregion
	}
}
