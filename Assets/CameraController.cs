﻿using UnityEngine;
using System.Collections;
using UnidentifiedFreakinObject.CapturePhoto.Scripts;

public class CameraController : MonoBehaviour
{
	public CameraDropdownController CameraDropDown;
	public CameraRenderer Camera;

	public void ShowWebCamDevice(string deviceName)
	{
		if (Camera != null)
			Camera.ShowWebCamDevice(deviceName);
	}
}
