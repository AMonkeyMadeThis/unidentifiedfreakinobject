﻿namespace AMonkeyMadeThis.Common.Model.DTO
{
	using System.Xml.Serialization;

	/**
	 * AbstractDTO
	 */
	public abstract class AbstractDTO
	{
		[XmlAttribute(AttributeName = "id")]
		/**
		 * ID
		 */
		public int ID = -1;
	}
}
