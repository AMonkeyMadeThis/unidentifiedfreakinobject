﻿namespace AMonkeyMadeThis.Common.Util
{
	using System;
	using System.Reflection;

	/**
	 * ObjectUtils
	 */
	public class ObjectUtils
	{
		/**
		 * HasOwnProperty
		 */
		public static bool HasOwnProperty(object target, string propertyName)
		{
			Type type = target.GetType();
			PropertyInfo property = type.GetProperty(propertyName);
			FieldInfo field = type.GetField(propertyName);
			bool result = (property != null) || (field != null);

			return result;
		}

		/**
		 * GetProperty
		 */
		public static object GetProperty(object target, string propertyName)
		{
			if (!HasOwnProperty(target, propertyName))
				return null;

			Type type = target.GetType();
			PropertyInfo property = type.GetProperty(propertyName);
			FieldInfo field = type.GetField(propertyName);

			if (property != null)
			{
				return property.GetValue(target, null);
			}
			else if (field != null)
			{
				return field.GetValue(target);
			}
			else
			{
				return null;
			}
		}
	}
}
