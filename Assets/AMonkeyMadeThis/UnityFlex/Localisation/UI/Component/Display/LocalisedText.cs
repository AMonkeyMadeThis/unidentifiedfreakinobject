﻿namespace AMonkeyMadeThis.UnityFlex.Localisation.UI.Component.Display
{
	using Controller;
	using UnityEngine;
	using UnityEngine.UI;
	using UnityFlex.UI.Component;

	[RequireComponent(typeof(Text))]
	/**
	 * LocalisedText
	 */
	public class LocalisedText : UIComponent
	{
		//[SerializeField]
		/**
		 * (default) ResourceBundle
		 */
		//private string _resourceBundle = "Application";

		/**
		 * ResourceBundle
		 */
		//public string ResourceBundle
		//{
		//	get { return _resourceBundle; }
		//	set
		//	{
		//		UnRegisterResourceBundleUse();
		//		_resourceBundle = value;
		//		RegisterResourceBundleUse();
		//		InvalidateProperties();
		//	}
		//}

		[SerializeField]
		/**
		 * (default) ResourceKey
		 */
		private string _resourceKey;

		/**
		 * ResourceKey
		 */
		public string ResourceKey
		{
			get { return _resourceKey; }
			set
			{
				_resourceKey = value;
				InvalidateProperties();
			}
		}
		
		/**
		 * Label
		 */
		protected Text Label;

		public override void OnEnable()
		{
			base.OnEnable();

			ClearLabelText();
		}

		public override void OnDisable()
		{
			base.OnDisable();
			
			ClearLabelText();
		}

		public override void Awake()
		{
			base.Awake();

			ClearLabelText();
		}

		/*
		public override void Start()
		{
			base.Start();

			InvalidateProperties();
		}
		*/

		/*
		private void RegisterResourceBundleUse()
		{
			if (ResourceBundle != null && ResourceBundle.Length > 0)
				LocalisationController.Instance.RegisterResourceBundleUse(ResourceBundle);
		}

		private void UnRegisterResourceBundleUse()
		{
			if (ResourceBundle != null && ResourceBundle.Length > 0)
				LocalisationController.Instance.UnRegisterResourceBundleUse(ResourceBundle);
		}
		*/

		protected override void CollectComponents()
		{
			base.CollectComponents();

			Label = GetComponent<Text>();
		}
		
		/*
		public override void AddListeners()
		{
			base.AddListeners();

			AddLocalisationListeners();
		}

		public override void RemoveListeners()
		{
			base.RemoveListeners();

			RemoveLocalisationListeners();
		}
		*/

		/*
		private void AddLocalisationListeners()
		{
			LocalisationController.OnLocaleChanged += HandleOnLocaleChanged;
		}

		private void RemoveLocalisationListeners()
		{
			LocalisationController.OnLocaleChanged -= HandleOnLocaleChanged;
		}

		private void HandleOnLocaleChanged()
		{
			InvalidateProperties();
		}
		*/

		protected override void CommitProperties()
		{
			UpdateLabelText();

			base.CommitProperties();
		}

		private void ClearLabelText()
		{
			if (Label != null)
				Label.text = string.Empty;
		}

		private void UpdateLabelText()
		{
			if (Label != null)
				Label.text = LocalisedLabelText;
		}

		protected virtual string LocalisedLabelText
		{
			get
			{
				string result = LocalisationController.Instance.LocaliseString(ResourceKey, ResourceBundle);
				return result;
			}
		}
	}
}
