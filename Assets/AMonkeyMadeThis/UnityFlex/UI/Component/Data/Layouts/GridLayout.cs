﻿namespace AMonkeyMadeThis.UnityFlex.UI.Component.Data.Layouts
{
    using UnityEngine;

    /**
     * GridLayout
     */
    public class GridLayout : Layout
    {
        /**
         * Columns
         */
        public int Columns = 5;
        /**
         * Rows
         */
        public int Rows = 3;
        /**
         * Scale
         */
        public float Scale = .75f;

        /**
         * AlignChild
         */
        protected override void AlignChild(Transform child, int index)
        {
            int column = index % Columns;
            int row = Mathf.FloorToInt(index / Columns);
            Vector3 position = new Vector3();
            float columnWidth = (2 * DataGroup.Width * Scale) / (Columns - 1);
            float rowHeight = (DataGroup.Height * Scale) / (Rows - 1);
            position.x = (-(DataGroup.Width * Scale) / 1) + (columnWidth * column);
            position.y = ((DataGroup.Height * Scale) / 2) - (rowHeight * row);

            child.localScale = Vector3.one;
            child.localRotation = Quaternion.Euler(0f, 0f, 0f);
            child.localPosition = position;
        }
    }
}
