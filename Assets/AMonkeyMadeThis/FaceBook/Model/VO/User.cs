﻿namespace AMonkeyMadeThis.FaceBook.Model.VO
{
	using Common.Model.VO;
	using Enum;
	using System.Collections.Generic;

	/**
	 * User
	 */
	public class User : ValueObject
	{
		public string ID;
		public List<Permission> Permissions;
		public List<User> Friends;
		public string FirstName;
	}
}
