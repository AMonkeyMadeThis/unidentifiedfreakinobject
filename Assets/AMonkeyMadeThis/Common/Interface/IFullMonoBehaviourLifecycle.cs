﻿namespace AMonkeyMadeThis.Common.Interface
{
	/**
	 * IFullMonoBehaviourLifecycle
	 * 
	 * @see http://docs.unity3d.com/Manual/ExecutionOrder.html
	 */
	public interface IFullMonoBehaviourLifecycle
	{
		#region Editor

		/**
		 * Reset is called to initialize the script’s properties when it is first attached to the object and also when the Reset command is used.
		 */
		void Reset();

		#endregion

		#region First Scene Load

		/**
		 * This function is always called before any Start functions and also just after a prefab is instantiated. (If a GameObject is inactive during start up Awake is not called until it is made active.)
		 */
		void Awake();

		/**
		 *  (only called if the Object is active): This function is called just after the object is enabled. This happens when a MonoBehaviour instance is created, such as when a level is loaded or a GameObject with the script component is instantiated.
		 */
		void OnEnable();

		/**
		 * This function is executed to inform the game that a new level has been loaded.
		 */
		void OnLevelWasLoaded();

		#endregion

		#region Before the first frame update

		/**
		 * Start is called before the first frame update only if the script instance is enabled.
		 */
		void Start();

		#endregion

		#region In between frames

		/**
		 * This is called at the end of the frame where the pause is detected, effectively between the normal frame updates. One extra frame will be issued after OnApplicationPause is called to allow the game to show graphics that indicate the paused state.
		 */
		void OnApplicationPause();

		#endregion

		#region Update Order
		
		/**
		 * FixedUpdate is often called more frequently than Update.It can be called multiple times per frame, if the frame rate is low and it may not be called between frames at all if the frame rate is high.All physics calculations and updates occur immediately after FixedUpdate.When applying movement calculations inside FixedUpdate, you do not need to multiply your values by Time.deltaTime.This is because FixedUpdate is called on a reliable timer, independent of the frame rate.
		 */
		void FixedUpdate();

		/**
		 * Update is called once per frame. It is the main workhorse function for frame updates.
		 */
		void Update();

		/**
		 * LateUpdate is called once per frame, after Update has finished. Any calculations that are performed in Update will have completed when LateUpdate begins.A common use for LateUpdate would be a following third-person camera. If you make your character move and turn inside Update, you can perform all camera movement and rotation calculations in LateUpdate.This will ensure that the character has moved completely before the camera tracks its position.
		 */
		void LateUpdate();

		#endregion

		#region Rendering
		
		/**
		 * Called before the camera culls the scene.Culling determines which objects are visible to the camera.OnPreCull is called just before culling takes place.
		 */
		void OnPreCull();

		/**
		 * Called when an object becomes visible to any camera.
		 */
		void OnBecameVisible();

		/**
		 * Called when an object becomes invisible to any camera.
		 */
		void OnBecameInvisible();

		/**
		 * Called once for each camera if the object is visible.
		 */
		void OnWillRenderObject();

		/**
		 * Called before the camera starts rendering the scene.
		 */
		void OnPreRender();

		/**
		 * Called after all regular scene rendering is done.You can use GL class or Graphics.DrawMeshNow to draw custom geometry at this point.
		 */
		void OnRenderObject();

		/**
		 * Called after a camera finishes rendering the scene.
		 */
		void OnPostRender();

		/**
		 * Called after scene rendering is complete to allow postprocessing of the image, see ImageEffects.
		 */
		void OnRenderImage(); 

		/**
		 * Called multiple times per frame in response to GUI events. The Layout and Repaint events are processed first, followed by a Layout and keyboard/mouse event for each input event.
		 */
		void OnGUI();

		/**
		 * Used for drawing Gizmos in the scene view for visualisation purposes.
		 */
		void OnDrawGizmos();

		#endregion

		#region When the Object is Destroyed

		/**
		 * This function is called after all frame updates for the last frame of the object’s existence(the object might be destroyed in response to Object.Destroy or at the closure of a scene).
		 */
		void OnDestroy();

		#endregion

		#region When Quitting
		
		/**
		 * This function is called on all game objects before the application is quit.In the editor it is called when the user stops playmode.In the web player it is called when the web view is closed.
		 */
		void OnApplicationQuit();
		/**
		 * This function is called when the behaviour becomes disabled or inactive.
		 */
		void OnDisable();

		#endregion
	}
}
