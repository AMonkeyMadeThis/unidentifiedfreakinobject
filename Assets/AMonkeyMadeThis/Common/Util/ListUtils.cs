﻿namespace AMonkeyMadeThis.Common.Util
{
	using System.Collections;
	using System.Collections.Generic;

	/**
	 * ListUtils
	 */
	public class ListUtils
	{
		public static List<object> MakeGeneric(IList source)
		{
			List<object> result = new List<object>();

			foreach (var item in source)
			{
				result.Add((object)item);
			}

			return result;
		}
	}
}
