﻿namespace AMonkeyMadeThis.UnityFlex.Localisation.UI.Component.Navigation
{
	using Display;
	using UnityFlex.UI.Component.Navigation;

	/**
	 * LocalisedNavBarButton
	 */
	public class LocalisedNavBarButton : NavBarButton
	{
		/**
		 * Label
		 */
		public LocalisedText LocalisedText;

		protected override void CollectComponents()
		{
			base.CollectComponents();

			LocalisedText = GetComponentInChildren<LocalisedText>();
		}
		
		protected override void SetLabelText()
		{
			if (LocalisedText != null)
				LocalisedText.ResourceKey = Text;
		}
	}
}
