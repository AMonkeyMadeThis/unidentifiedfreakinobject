﻿namespace AMonkeyMadeThis.CognitiveServices.Vision.Analyze.Service
{
	using Common.Service;
	using Enum;
	using Model;
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityFlex.Network;

	/**
	 * AnalyzeService
	 */
	public class AnalyzeService : AbstractService
	{
		#region Singleton

		private static AnalyzeService _instance;

		public static AnalyzeService Instance
		{
			get { return _instance; }
			private set
			{
				_instance = value;
			}
		}

		#endregion

		#region CTOR

		public AnalyzeService()
		{
			Instance = this;
		}

		#endregion

		#region Consts

		public const string SUBSCRIPTION_KEY_HEADER_NAME = "Ocp-Apim-Subscription-Key";

		#endregion

		#region Member vars

		public VisualFeatures[] Features;
		public string UploadURL = "https://api.projectoxford.ai/vision/v1.0/analyze?visualFeatures=Categories,Tags,Description,Faces";
		public string SubscriptionKey = "d735b2d76b944b30969451c0f233a850";
		public string FieldName = "file";
		public string FileName = "0001.png";
		public string MimeType = "image/png";

		#endregion

		#region Upload Image

		public void Post(Texture2D image, Responder responder)
		{
			DevUpload(image, responder);
			//StartCoroutine(Upload(image, responder));
		}

		private string jsonData = "{\"categories\":[{\"name\":\"people_portrait\",\"score\":0.96484375}],\"tags\":[{\"name\":\"person\",\"confidence\":0.999969482421875},{\"name\":\"man\",\"confidence\":0.99992239475250244},{\"name\":\"wall\",\"confidence\":0.99916410446166992},{\"name\":\"indoor\",\"confidence\":0.9934651255607605},{\"name\":\"shirt\",\"confidence\":0.86541134119033813},{\"name\":\"looking\",\"confidence\":0.82180851697921753},{\"name\":\"male\",\"confidence\":0.15631875395774841}],\"description\":{\"tags\":[\"person\",\"man\",\"indoor\",\"shirt\",\"looking\",\"holding\",\"camera\",\"wearing\",\"glasses\",\"front\",\"standing\",\"young\",\"hand\",\"posing\",\"white\",\"food\",\"smiling\",\"remote\",\"black\",\"red\",\"room\",\"video\",\"computer\"],\"captions\":[{\"text\":\"a man in a shirt and tie looking at the camera\",\"confidence\":0.45553532045405909}]},\"requestId\":\"f77c49a0-b1cb-4ecb-b350-2a219247a7f1\",\"metadata\":{\"width\":1920,\"height\":1080,\"format\":\"Png\"},\"faces\":[{\"age\":33,\"gender\":\"Male\",\"faceRectangle\":{\"left\":908,\"top\":305,\"width\":457,\"height\":457}}]}";

		private void DevUpload(Texture2D image, Responder responder)
		{
			var result = JsonUtility.FromJson<AnalyseResult>(jsonData);

			SendResult(responder, result);
		}
		
		private IEnumerator Upload(Texture2D image, Responder responder)
		{
			WWWForm postForm = new WWWForm();
			byte[] bytes = image.EncodeToPNG();
			postForm.AddBinaryData(FieldName, bytes, FileName, MimeType);

			Dictionary<string, string> headers = postForm.headers;
			headers[SUBSCRIPTION_KEY_HEADER_NAME] = SubscriptionKey;

			WWW upload = new WWW(UploadURL, postForm.data, headers);
			yield return upload;
			
			if (upload.error == null && !string.IsNullOrEmpty(upload.text))
			{
				var result =  JsonUtility.FromJson<AnalyseResult>(upload.text);

				SendResult(responder, result);
			}
			else
			{
				SendFault(responder);
			}
		}

		#endregion

		/*
		{
		"categories":[{"name":"abstract_","score":0.015625},{"name":"others_","score":0.01953125}],
		"tags":[{"name":"indoor","confidence":0.88742184638977051}],
		"description":{
			"tags":["indoor","sitting","table","computer","desk","bicycle","laptop","white","top","small","man","mouse","pizza","room","standing","keyboard","woman","kitchen","laying","phone","oven","cat"],
			"captions":[{"text":"a laptop computer","confidence":0.40524548841444563}]
		},
		"requestId":"9646076a-4dbc-4fbc-a771-e88a943022a6",
		"metadata":{"width":1920,"height":1080,"format":"Png"},
		"faces":[]}
		*/

		/*
		{
		"categories":[{"name":"people_portrait","score":0.96484375}],
		"tags":[{"name":"person","confidence":0.999969482421875},{"name":"man","confidence":0.99992239475250244},{"name":"wall","confidence":0.99916410446166992},{"name":"indoor","confidence":0.9934651255607605},{"name":"shirt","confidence":0.86541134119033813},{"name":"looking","confidence":0.82180851697921753},{"name":"male","confidence":0.15631875395774841}],
		"description":{
			"tags":["person","man","indoor","shirt","looking","holding","camera","wearing","glasses","front","standing","young","hand","posing","white","food","smiling","remote","black","red","room","video","computer"],
			"captions":[{"text":"a man in a shirt and tie looking at the camera","confidence":0.45553532045405909}]},
		"requestId":"f77c49a0-b1cb-4ecb-b350-2a219247a7f1",
		"metadata":{"width":1920,"height":1080,"format":"Png"},
		"faces":[{"age":33,"gender":"Male","faceRectangle":{"left":908,"top":305,"width":457,"height":457}}]}
		*/
	}
}
