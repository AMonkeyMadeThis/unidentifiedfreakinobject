﻿namespace AMonkeyMadeThis.UnityFlex.UI.Component
{
	using AMonkeyMadeThis.Common.Util;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.UI;
	using System;

	[RequireComponent(typeof(Dropdown))]
	/**
	 * LocaleDropDownController
	 */
	public class DropDownController : DataController
	{
		#region Member vars

		/**
		 * Dropdown
		 */
		public Dropdown Dropdown;
		/**
		 * LabelField, the field to be used as a label when adding the contents of the datasource to the dropdown's options. If empty then object.ToString() will be used instead.
		 */
		public string LabelField;
		/**
		 * HasLabelField
		 */
		public bool HasLabelField
		{
			get { return !string.IsNullOrEmpty(LabelField); }
		}
		/**
		 * ImageField, the field to be used as an image when adding the contents of the datasource to the dropdown's options.
		 */
		public string ImageField;
		/**
		 * HasImageField
		 */
		public bool HasImageField
		{
			get { return !string.IsNullOrEmpty(ImageField); }
		}

		#endregion

		#region Initialisation

		/**
		 * Awake
		 */
		public override void Awake()
		{
			base.Awake();

			PopulateDropdown();
			MakeInitialSelection();
		}

		/**
		 * CollectComponents
		 */
		protected override void CollectComponents()
		{
			base.CollectComponents();

			Dropdown = GetComponent<Dropdown>();
		}

		/**
		 * Reset
		 */
		protected override void Reset()
		{
			base.Reset();

			PopulateDropdown();
		}

		#endregion

		#region Listeners

		/**
		 * AddListeners
		 */
		public override void AddListeners()
		{
			base.AddListeners();
			
			Dropdown.onValueChanged.AddListener(delegate { HandleDropDownValueChanged(); });
		}

		#endregion

		#region Handlers

		/**
		 * HandleDropDownValueChanged
		 */
		protected virtual void HandleDropDownValueChanged()
		{
			Debug.Log(this + " HandleDropDownValueChanged");

			int index = Dropdown.value;
			SelectedIndex = index;
		}

		#endregion

		#region Population

		/**
		 * PopulateDropdown
		 */
		protected virtual void PopulateDropdown()
		{
			if (Dropdown != null && Datasource != null)
			{
				List<Dropdown.OptionData> options = new List<Dropdown.OptionData>();

				foreach (var item in Datasource)
				{
					Dropdown.OptionData option = new Dropdown.OptionData();
					option.text = GetItemLabel(item);
					option.image = GetItemImage(item);
					//string label = GetItemLabel(item);

					options.Add(option);
				}

				Dropdown.ClearOptions();
				Dropdown.AddOptions(options);
			}
		}

		/**
		 * GetItemLabel
		 */
		protected virtual string GetItemLabel(object item)
		{
			if (!HasLabelField || !ObjectUtils.HasOwnProperty(item, LabelField))
				return item.ToString();

			object value = ObjectUtils.GetProperty(item, LabelField);
			string result = value.ToString();

			return result;
		}

		/**
		 * GetItemImage
		 */
		protected virtual Sprite GetItemImage(object item)
		{
			if (!HasImageField || !ObjectUtils.HasOwnProperty(item, ImageField))
				return null;

			object value = ObjectUtils.GetProperty(item, ImageField);
			Sprite result = value as Sprite;

			return result;
		}

		#endregion

		#region Selection

		protected virtual void MakeInitialSelection()
		{
			// template method
		}

		protected override void UpdateSelectedIndex()
		{
			base.UpdateSelectedIndex();

			Dropdown.value = SelectedIndex;
		}

		#endregion
	}
}
