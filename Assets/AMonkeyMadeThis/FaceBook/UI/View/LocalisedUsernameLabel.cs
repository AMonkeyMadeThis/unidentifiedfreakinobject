﻿namespace AMonkeyMadeThis.FaceBook.UI.View
{
	using Model;
	using Model.VO;
	using System;
	using UnityFlex.Localisation.UI.Component.Display;

	/**
	 * LocalisedUsernameLabel
	 */
	public class LocalisedUsernameLabel : LocalisedText
	{
		public override void OnEnable()
		{
			base.OnEnable();

			AddModelListeners();
		}

		public override void OnDisable()
		{
			base.OnDisable();

			RemoveModelListeners();
		}

		private void AddModelListeners()
		{
			FaceBookModel.UserChanged += HandleUserChange;
		}

		private void RemoveModelListeners()
		{
			FaceBookModel.UserChanged -= HandleUserChange;
		}

		private void HandleUserChange(User user)
		{
			InvalidateProperties();
		}

		protected override string LocalisedLabelText
		{
			get
			{
				string localised = base.LocalisedLabelText;
				string username = (FaceBookModel.Instance.User != null) ? FaceBookModel.Instance.User.FirstName : string.Empty;
				string result = string.Format(localised, username);

				return result;
			}
		}
	}
}
