﻿namespace AMonkeyMadeThis.FaceBook.Enum
{
	using System;

	[Flags]
	/**
	 * Permission
	 */
	public enum Permission
	{
		public_profile,
		email,
		user_friends
	}
}
