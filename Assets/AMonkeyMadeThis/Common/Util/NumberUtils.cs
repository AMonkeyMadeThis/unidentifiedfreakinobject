﻿namespace AMonkeyMadeThis.Common.Util
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Text;
	using UnityEngine;

	/**
	 * NumberUtils
	 */
	public class NumberUtils
	{
		public static string PercentageFormat(float value, int precision = 2)
		{
			float multiplied = value * 100;
			string formatted = Format(multiplied, precision);

			return formatted;
		}

		public static string Format(float value, int precision=2)
		{
			float rounded = FloorToPrecision(value, precision);
			string padded = rounded.ToString();

			return padded;
		}

		public static float FloorToPrecision(float value, int precision)
		{
			float scalar = Mathf.Pow(10, precision);
			float result = Mathf.Floor(value * scalar) / scalar;

			return result;
		}

		public static float RoundToPrecision(float value, int precision)
		{
			float scalar = Mathf.Pow(10, precision);
			float result = Mathf.Round(value * scalar) / scalar;

			return result;
		}

		public static string PadToPrecision(float value, int precision)
		{
			string strValue = value.ToString();
			string[] arrValue = strValue.Split('.');
			string integerPart = (arrValue.Length > 0) ? arrValue[0] : "0";
			string fractionalPart = (arrValue.Length > 1) ? arrValue[1] : string.Empty;

			while (fractionalPart.Length < precision)
			{
				fractionalPart = fractionalPart + "0";
			}

			string pattern = (precision > 0) ? "{0}.{1}" : "{0}";
			string result = string.Format(pattern, integerPart, fractionalPart);

			return result;
		}

		public static float GetMinorFraction(float value, int precision)
		{
			float scalar = Mathf.Pow(10, precision + 1);
			float floor = FloorToPrecision(value, precision);
			float floorInt = Mathf.FloorToInt(floor * scalar);
			int valueInt = Mathf.FloorToInt(value * scalar);
			float result = valueInt - floorInt;

			return result;
		}
	}
}
